-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 31 Jul 2018 pada 14.32
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_santosahospital`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_dokter`
--

CREATE TABLE `tb_dokter` (
  `id_spesialis` int(11) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `nama_dokter` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_dokter`
--

INSERT INTO `tb_dokter` (`id_spesialis`, `id_dokter`, `nama_dokter`) VALUES
(1, 1, 'Indriawati,dr.'),
(1, 2, 'Riksma Nurahmi R,A.,dr.,MPd'),
(2, 3, 'Maharani Risiska Utami,dr.'),
(3, 4, 'Amanda Ocatacia Sjam.Spsi,Msi'),
(3, 5, 'Cakrangadinata,M.Psi,Psik'),
(4, 6, 'Sherlly Surijadi,Sp.AK, MARs'),
(4, 7, 'Stefanus Agung,dr., Sp.Ak'),
(5, 8, 'Ariantana,dr.,SpA'),
(5, 9, 'Suzy Irawati Sjahid,dr.,SpA'),
(5, 10, 'Iva Noviana Fitrya,dr.,SpA,Mkes'),
(5, 11, 'Prihariadi R. Poedjiadi,dr.,SpA'),
(5, 12, 'Widjajaningsih,dr.,SpA'),
(5, 13, 'Lelani Reniarti,dr,Sp.A(K) - Konsultan hematologi'),
(5, 14, 'Ina Rosalina,dr.,SpA(K)'),
(5, 15, 'Rahmat Budi Kuswiyanto,dr.,SpA(K),Mkes'),
(5, 16, 'Dikki Drajat,dr.,SpB SpBA'),
(5, 17, 'Edward Plentinus Simamora,dr.,SpBA'),
(5, 18, 'Emiliana Lia, dr.,Sp.BA'),
(6, 19, 'Ina Rosalina,dr.,SpA(K)'),
(7, 20, 'Rahmat Budi Kuswiyanto,dr.,SpA(K),Mkes'),
(8, 21, 'Dikki Drajat,dr.,SpB'),
(8, 22, 'Edward Plentinus Simamora,dr.,SpBA'),
(8, 23, 'Emiliana Lia, dr., Sp.BA'),
(9, 24, 'Drajat R Suardi,dr.,SpB(K)Onk'),
(9, 25, 'Syafwan Adenan,dr.,SpB(K)Onk'),
(9, 26, 'Fransisca Badudu,dr.,SpB(K)Onk'),
(9, 27, 'Maman Abdurahman,dr.,SpB(K)Onk'),
(10, 28, 'Daniel Pratikno,dr.,SpOT'),
(10, 29, 'Wahyu Riansyah,dr.,SpOT,Mkes'),
(10, 30, 'Husodo Dewo Adi,dr.,SpOT-Spine'),
(10, 31, 'Harry Kamijantono,dr.,SpOT'),
(10, 32, 'Fachry A Tandjung,Prof.,Dr.,dr.,SpB,SpOT,M.Phil'),
(10, 33, 'Yoyos,dr.,SpOT'),
(10, 34, 'Ahmad Ramdan,dr.,SpOT.,Spine'),
(10, 35, 'Dicky Mulyadi,dr.,SpOT'),
(10, 36, 'Azeta Arif,dr,SpOT(K),Mkes'),
(10, 37, 'Setyagung B. Santoso, dr.,Sp.OT'),
(10, 38, 'Dadang Rukanta, dr., Sp.OT, M.Kes'),
(11, 39, 'Agus Nurtadwiyana,dr.,SpB-KBD'),
(11, 40, 'Lukamana Lokarjana, dr, SpB-KBD'),
(11, 41, 'nurhayat Usman,dr.,SpB-KBD,FinaCS'),
(11, 42, 'Andriana Purnama,dr.,SpB-KBD'),
(11, 43, 'Basrul Hanafi,Prof.,dr.,SpB-KBD'),
(12, 44, 'Almahitta Cintami Putri,dr.,SpBP-RE'),
(12, 45, 'Ali Sundoro,dr.,Sp.BP-RE(K)'),
(12, 46, 'Hardisiswo Soedjana,DR.,dr.,Sp.BP-RE(K)'),
(12, 47, 'Irra Rubianti,dr.,Sp.BP-RE(K)'),
(12, 48, 'Lisa Hasibuan,dr.,Sp.BP-RE (K)'),
(12, 49, 'Rani Septrina, dr.,Sp.BP-RE'),
(13, 50, 'Benny Atmaja W.,dr.,SpBS(K)'),
(13, 51, 'Halim Achmad,dr.SpBS'),
(13, 52, 'Andri Anugerah,dr.,SpBS,Mkes'),
(13, 53, 'Firman Priguna Tjahjono,dr.,SpBS,Mkes'),
(13, 54, 'Mirna Sobarna,dr.,SpBS,Mkes'),
(13, 55, 'Sobarna,dr.,SpBS,Mkes'),
(13, 56, 'Agung Budi Setiono, dr.,SpBS'),
(14, 57, 'Saladdin Tjokronegoro,dr.,SpB-TKV'),
(14, 58, 'Rama Nursjirwan,dr.,SpB-TKV'),
(14, 59, 'Putie Hapsari,dr.,SpB(K)V'),
(14, 60, 'Rahim Sobana, dr.,Sp.B-TKV'),
(14, 61, 'Tri Wahyu,DR.,dr.,Sp.B-TKV'),
(15, 62, 'James Mandolang,dr.,SpB,FlnaCS'),
(15, 63, 'Timotius Tedjajuwana, dr.,Sp.B'),
(15, 64, 'Dino Adrian Halim,dr.,SpB,D.MAS'),
(15, 65, 'Danny Ganiatro Sugandi,dr.,SpB'),
(16, 66, 'Ronald R. Rusman,dr.,SpU'),
(16, 67, 'Kuncoro Adi,dr.,SpU'),
(17, 68, 'Maya Mukti Sari,drg.'),
(17, 69, 'Yunita Dewi,drg.,SpKG'),
(17, 70, 'Lena Lia Napitupulu,drg.'),
(17, 71, 'Dyah Hajuni Ambasrsari,drg.,SpKG'),
(17, 72, 'Maya S. Prabowo,drg.,SpProst'),
(17, 73, 'Rahmat Babuta,drg.,SpBM'),
(17, 74, 'Seto Adiantoro Sadputranto,drg.,Sp.BM'),
(17, 75, 'Marlyn Artalina,drg.,SpKGA'),
(17, 76, 'Hendry Muchtasar,drg.,SpOrt'),
(17, 77, 'Helmi Siti Aminah,drg.,SpProst'),
(17, 78, 'Novita Sukma,drg.,SpKGA'),
(18, 79, 'A. Purba, Prof, Dr, dr, Msc, AIFO'),
(19, 80, 'Salli Fitriyanti,dr.,M.Gizi,SpGK'),
(19, 81, 'Nurly Hestika Wardhani,dr.,M.Gizi,SpGK'),
(20, 82, 'Enday Sukandar,Prof.Dr.dr.Sp.PD-KGH'),
(20, 83, 'Afiantin,dr.,SpPD-KGH'),
(21, 84, 'rACHMAT s sUMANTRI,DR.,sPpd-KHOM'),
(21, 85, 'Pandji Irani Fianza,dr,Sp.PD-KHOM'),
(22, 86, 'Riardi Pramudiyo,dr.,SpPD-KR(A)'),
(23, 87, 'Adhiarta,dr.,SpPD-KEMD'),
(23, 88, 'Octo Indradjaja,dr.,SpPD-KEMD'),
(24, 89, 'Dolvi Girawan,dr.,SpPD'),
(24, 90, 'Nenny Agustanti, SpPD-KGEH'),
(24, 91, 'Julianto Widjojo, Prof, MD, Sp.PD-KGEH, FINASIM, AGAF'),
(24, 92, 'Juke Roslia Sakti, dr., Sp.PD-KGEH'),
(25, 93, 'Robert Hendrik Siahaan,dr.,SpOG,Mkes'),
(25, 94, 'I.M. Yoga Adhiyajna,dr.,SpOG,Mkes'),
(25, 95, 'Marissa Tasya,dr.,SpOG,Mkes'),
(25, 96, 'Hanny Rono,dr.,SpOG(K),MM'),
(25, 97, 'Hendra Gunawan W.,dr.,SpOG'),
(25, 98, 'Hj. Muzayanah, SpOG'),
(25, 99, 'Herman Susanto,dr.,Prof.,SpOG(K)'),
(25, 100, 'Achmad Suardi,dr.,SpOG(K)'),
(25, 101, 'Supriadi Gandamihardja,dr.,SpOG(K)'),
(25, 102, 'Dodi Suardi,dr,Sp.OG'),
(26, 103, 'Rachmat Purwata,dr.,SpKJ'),
(26, 104, 'Santi Andayani,dr.,SpKJ'),
(26, 105, 'Lelly Resna,dr.SpKJ(K)'),
(27, 106, 'Musliani Moestopo,dr.,SpKK'),
(27, 107, 'Reni Wiyakti Nanda Dewi,dr.,SpKK'),
(27, 108, 'Mariza Febyani,dr.,Sp.KK'),
(27, 109, 'Liem Fenny,dr.,SpKK'),
(27, 110, 'Lia Marlia, SpKK,dr.,M.Kes'),
(28, 111, 'Anriafi Syah,dr.,SpM'),
(28, 112, 'Abraham A. Sutjiono,dr.,SpM,FIACLE'),
(28, 113, 'Chalid Kurniawan,dr.,SpM,Mkes'),
(28, 114, 'Rinaldi Dahlan,dr.,Sp.M'),
(28, 115, 'Helini Sari Lubis,dr.,Sp.M'),
(28, 116, 'Laksmi Thaufiq,dr.,SpM(K)'),
(28, 117, 'Arief S.Kartasasmita,dr,Sp.M'),
(28, 118, 'Katharina Willyasti,dr,SpM'),
(28, 119, 'Pratiwi Yuliandari,dr.,SpM,Mkes'),
(28, 120, 'Irawati Irfani,dr,SpM, M.Kes'),
(29, 121, 'Anang Achmadi,dr.,Sp.An-KIC'),
(29, 122, 'Suwarman,dr.,Sp.An,KIC,KMN'),
(29, 123, 'M. Andy Prihartono, Sp.An, M.Kes,KMN,FIPM'),
(29, 124, 'Condrad M.P. Pasaribu, dr., Sp.S, FINS'),
(29, 125, 'Toto Tanumihardja,dr.,Sp.KFR'),
(29, 126, 'Azeta Arif, dr., Sp.OT(K)'),
(30, 127, 'Syarifudin,dr.,SpP'),
(30, 128, 'Benyamin J.T.,dr.,SpP,FCCP'),
(30, 129, 'HR.Nana Sundaraja,dr.,SpP'),
(30, 130, 'Reza Kurniawan Tanuwiharja,dr.,Sp.P'),
(31, 131, 'Perry Rachmat Permana,dr.,SpPD'),
(31, 132, 'Dede Budiman,dr.,SpPD,Mkes'),
(31, 133, 'Fifi Akwarini,dr., SpPD'),
(31, 134, 'Nieke Dewi R.K.,dr.,SpPD'),
(31, 135, 'Anggaraini W.,dr.,SpPD'),
(31, 136, 'Uun Sumardi,dr.,SpPD'),
(31, 137, 'R.Beni Benardi,dr.,SpPD'),
(31, 138, 'Imelda Adrianti Widjojo,Sp.PD'),
(31, 139, 'M. M. Moeliono, dr., Sp.PD'),
(32, 140, 'Eppy Buchory,dr., Sp.Rad'),
(32, 141, 'Hilman,dr.,Sp.Rad'),
(33, 142, 'Toto Tanumihardja,dr.,SpRM'),
(33, 143, 'Sari Dewi Saraswati,dr.,SpRM'),
(34, 144, 'Benny Atmaja W.,dr.,SpBS(K)'),
(34, 145, 'Halim Achmad,dr.,SpBS'),
(34, 146, 'Andri Anugerah,dr.,SpBS,Mkes'),
(34, 147, 'Firman Priguna Tjahjono,dr.,SpBS,Mkes'),
(34, 148, 'Mirna Sobarna,dr.,SpBS,Mkes'),
(34, 149, 'Agung Budi Setiono,dr.,SpBS'),
(34, 150, 'Jane Margaretha,dr.,SpS'),
(34, 151, 'Ela Kustila,dr.,SpS'),
(34, 152, 'Condrad M.P.Pasaribu,dr.,SpS'),
(34, 153, 'Nushrotul Lailiyya D.,dr.,SpS'),
(34, 154, 'Lisda Amalia,dr.,Sp.S'),
(35, 155, 'Lisa Syartika,dr.,MKes,SpTHT-KL'),
(35, 156, 'Teti Madiadipoera,Prof.,Dr.,dr.,SpTHT-KL'),
(35, 157, 'Nur Akbar Aroeman,dr.,SpTHT-KL'),
(35, 158, 'Lina Lasminingrum,dr.,MKes,SpTHT-KL'),
(35, 159, 'Imam Megantara,dr.,MKes,SpMK,SpTHT-KL'),
(35, 160, 'Melati Sudiro,dr.,MKes,SpTHT-KL'),
(35, 161, 'Yussy Afriani D.,dr.,MKes,SpTHT-KL'),
(35, 162, 'M. Indra Sapta,dr.,SpTHT-KL'),
(36, 163, 'A. Fauzi Yahya,dr.,Sp.JP'),
(36, 164, 'Rahmat Budi Kuswiyanto,dr.,SpA(K),Mkes'),
(36, 165, 'Soegeng Njotoprajitno,dr.,SpJP'),
(36, 166, 'Kiki Abdurachim Nazir,dr.,SpJP'),
(36, 167, 'Januar W.M.,drSpPD,SpJP,MHA,MPH'),
(36, 168, 'H.M. Agus Thosin,dr,SpJP'),
(36, 169, 'Prihati Pujowaskito,Sp.JP(k)'),
(37, 170, 'Herman Susanto,dr.,Prof.,SpOG(K)'),
(37, 171, 'Achmad Suardi,dr.,SpOG(K)'),
(37, 172, 'Supriadi Gandamihardja,dr.,SpOG(K)'),
(37, 173, 'Dodi Suardi,dr,Sp,OG');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pendaftaran`
--

CREATE TABLE `tb_pendaftaran` (
  `id_pendaftaran` int(11) NOT NULL,
  `status_id` varchar(16) NOT NULL,
  `no_id` int(11) NOT NULL,
  `namalengkap` varchar(50) NOT NULL,
  `namapanggilan` varchar(50) NOT NULL,
  `tmptlahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `gol_dar` int(11) NOT NULL,
  `jk` int(11) NOT NULL,
  `agama` int(11) NOT NULL,
  `status_diri` int(11) NOT NULL,
  `kewarganegaraan` int(11) NOT NULL,
  `pendidikan` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `kelurahan` text NOT NULL,
  `kecamatan` text NOT NULL,
  `kabupaten` text NOT NULL,
  `kode_pos` int(5) NOT NULL,
  `propinsi` text NOT NULL,
  `no_rumah` int(11) NOT NULL,
  `no_hp` int(11) NOT NULL,
  `pekerjaan` int(11) NOT NULL,
  `status_penjamin` int(11) NOT NULL,
  `no_penjamin` varchar(20) NOT NULL,
  `nama_penjamin` varchar(50) NOT NULL,
  `hubungan_penjamin` varchar(50) NOT NULL,
  `alamat_penjamin` varchar(50) NOT NULL,
  `notelp_penjamin` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_penjadwalan`
--

CREATE TABLE `tb_penjadwalan` (
  `id_penjadwalan` int(11) NOT NULL,
  `id_poli` int(11) NOT NULL,
  `nama_dokter` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `jdwl_praktek` varchar(30) NOT NULL,
  `jam_praktek` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_spesialis`
--

CREATE TABLE `tb_spesialis` (
  `id_spesialis` int(11) NOT NULL,
  `jenis_spesialis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_spesialis`
--

INSERT INTO `tb_spesialis` (`id_spesialis`, `jenis_spesialis`) VALUES
(1, 'Dokter umum'),
(2, 'Medical Check Up'),
(3, 'Psikolog'),
(4, 'Spesialis Akupuntur'),
(5, 'Spesialis Anak'),
(6, 'Spesialis Anak Konsultan Gastro Hepatologi'),
(7, 'Spesialis Anak Konsultan Jantung Anak'),
(8, 'Spesialis Bedah Anak'),
(9, 'Spesialis Bedah Onkologi'),
(10, 'Spesialis Bedah Ortopedi & Traumatologi'),
(11, 'Spesialis Bedah Pencernaan'),
(12, 'Spesialis Bedah Plastik'),
(13, 'Spesialis Bedah Saraf'),
(14, 'Spesialis bedah Thorax & Cardiovascular'),
(15, 'Spesialis Bedah Umum'),
(16, 'Spesialis Bedah Urologi'),
(17, 'Spesialis Dokter Gigi'),
(18, 'Spesialis Fisiologi Olahraga'),
(19, 'Spesialis Gizi Klinik'),
(20, 'Spesialis Internist Ginjal dan Hipertensi'),
(21, 'Spesialis Internist Onkologi-Hematologi'),
(22, 'Spesialis Internist Rheumatologi'),
(23, 'Spesialis Internist endokrin-metabolik-diabetes'),
(24, 'Spesialis Internist gastro-entero-hepatologi'),
(25, 'Spesialis Kandungan'),
(26, 'Spesialis Kesehatan Jiwa-Psychiatry'),
(27, 'Spesialis Kulit & Kecantikan'),
(28, 'Spesialis Mata'),
(29, 'Spesialis Nyeri Intervensi'),
(30, 'Spesialis Paru'),
(31, 'Spesialis Penyakit Dalam'),
(32, 'Spesialis Rasiologi Intervensi'),
(33, 'Spesialis Rehabilitasi Medik'),
(34, 'Spesialis Saraf'),
(35, 'Spesialis THT'),
(36, 'Spesialis Jantung'),
(37, 'Spesialis Kandungan Konsultan Onkologi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_dokter`
--
ALTER TABLE `tb_dokter`
  ADD PRIMARY KEY (`id_dokter`);

--
-- Indexes for table `tb_pendaftaran`
--
ALTER TABLE `tb_pendaftaran`
  ADD PRIMARY KEY (`id_pendaftaran`);

--
-- Indexes for table `tb_penjadwalan`
--
ALTER TABLE `tb_penjadwalan`
  ADD PRIMARY KEY (`id_penjadwalan`);

--
-- Indexes for table `tb_poli`
--
ALTER TABLE `tb_poli`
  ADD PRIMARY KEY (`id_poli`);

--
-- Indexes for table `tb_spesialis`
--
ALTER TABLE `tb_spesialis`
  ADD PRIMARY KEY (`id_spesialis`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_dokter`
--
ALTER TABLE `tb_dokter`
  MODIFY `id_dokter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT for table `tb_pendaftaran`
--
ALTER TABLE `tb_pendaftaran`
  MODIFY `id_pendaftaran` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_penjadwalan`
--
ALTER TABLE `tb_penjadwalan`
  MODIFY `id_penjadwalan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_poli`
--
ALTER TABLE `tb_poli`
  MODIFY `id_poli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_spesialis`
--
ALTER TABLE `tb_spesialis`
  MODIFY `id_spesialis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
