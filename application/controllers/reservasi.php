<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tb_pendaftaran
 * @created on : Monday, 09-Jul-2018 14:13:07
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class reservasi extends MY_Controller
{

    public function __construct() 
    { 
        parent::__construct();         
        $this->load->model('reservasis');
    }
    

    /**
    * List all data tb_pendaftaran
    *
    */
    public function index() 
    {
       
    $data['idk'] = $this->reservasis->get_gori();
    $data['idka'] = $this->reservasis->get_gori2();
    $this->load->view('reservasi',$data);
	      
    }

  
    public function save() 
    {            
        // if id NULL then add new data
    
                     
                          
                          $this->daftars->save();
                          $this->session->set_flashdata('notif', notify('Data berhasil di simpan','success'));
                          redirect('berobat');
    }

}

?>
