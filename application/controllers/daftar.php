<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tb_pendaftaran
 * @created on : Monday, 09-Jul-2018 14:13:07
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class daftar extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('daftars');
    }
    

    /**
    * List all data tb_pendaftaran
    *
    */
    public function index() 
    {
       
        $this->load->view('berobat');
	      
    }

  
    public function save() 
    {            
        // if id NULL then add new data
    
                     
                          
                          $this->daftars->save();
                          $this->session->set_flashdata('notif', notify('Data berhasil di simpan','success'));
                          redirect('berobat');
    }

}

?>
