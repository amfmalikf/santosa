<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_pendaftaran
 * @created on : Monday, 09-Jul-2018 14:13:08
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class daftars extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'status_id' => strip_tags($this->input->post('status_id', TRUE)),
        
            'namalengkap' => strip_tags($this->input->post('namalengkap', TRUE)),
        
            'namapanggilan' => strip_tags($this->input->post('namapanggilan', TRUE)),
        
            'tmptlahir' => strip_tags($this->input->post('tmptlahir', TRUE)),
        
            'tgl_lahir' => strip_tags($this->input->post('tgl_lahir', TRUE)),
        
            'gol_dar' => strip_tags($this->input->post('gol_dar', TRUE)),
        
            'jk' => strip_tags($this->input->post('jk', TRUE)),
        
            'agama' => strip_tags($this->input->post('agama', TRUE)),
        
            'status_diri' => strip_tags($this->input->post('status_diri', TRUE)),
        
            'kewarganegaraan' => strip_tags($this->input->post('kewarganegaraan', TRUE)),
        
            'alamat' => strip_tags($this->input->post('alamat', TRUE)),
        
            'kelurahan' => strip_tags($this->input->post('kelurahan', TRUE)),
        
            'kecamatan' => strip_tags($this->input->post('kecamatan', TRUE)),
        
            'kabupaten' => strip_tags($this->input->post('kabupaten', TRUE)),
        
            'kode_pos' => strip_tags($this->input->post('kode_pos', TRUE)),
        
            'propinsi' => strip_tags($this->input->post('propinsi', TRUE)),
        
            'no_rumah' => strip_tags($this->input->post('no_rumah', TRUE)),
        
            'no_hp' => strip_tags($this->input->post('no_hp', TRUE)),
        
            'pekerjaan' => strip_tags($this->input->post('pekerjaan', TRUE)),
        
            'status_penjamin' => strip_tags($this->input->post('status_penjamin', TRUE)),
        
            'nama_penjamin' => strip_tags($this->input->post('nama_penjamin', TRUE)),
        
            'hubungan_penjamin' => strip_tags($this->input->post('hubungan_penjamin', TRUE)),
        
            'alamat_penjamin' => strip_tags($this->input->post('alamat_penjamin', TRUE)),
        
            'notelp_penjamin' => strip_tags($this->input->post('notelp_penjamin', TRUE)),
        
        );
        
        
        $this->db->insert('tb_pendaftaran', $data);
    }

}
?>