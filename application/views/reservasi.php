<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Santosa Hospital</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by QBOOTSTRAP.COM" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="QBOOTSTRAP.COM" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by QBOOTSTRAP.COM
		
	Website: 		http://qbootstrap.com/
	Email: 			info@qbootstrap.com
	Twitter: 		http://twitter.com/Q_bootstrap
	Facebook: 		https://www.facebook.com/Qbootstrap
	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/animate.css") ?>">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/icomoon.css")?>">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/bootstrap.css")?>">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/magnific-popup.css")?>">
	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/owl.carousel.min.css")?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/owl.theme.default.min.css")?>">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/flexslider.css")?>">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/style.css")?>">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url("assets/plugin/js/modernizr-2.6.2.min.js")?>"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url("application/view/ajax_daerah.js")?>"></script> -->

	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	 <style>
      #map-canvas {width:100%;height:400px;border:solid #999 1px;}
      select {width:240px;}
      #kab_box,#kec_box,#kel_box,#lat_box,#lng_box{display:none;}
     </style>

	</head>
	<body>


		<!-- Info Head -->
	<div class="qbootstrap-loader"></div>
	
	<div id="page">
	<nav class="qbootstrap-nav" role="navigation">
		<div class="top-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="top">
							<div class="row">
								<div class="col-md-4 col-md-push-4 text-center">
									<div id="qbootstrap-logo"><a href="<?php echo site_url('proses/');?>"><i class="icon-plus-outline"></i>Santosa<span>care</span></a></div>
								</div>
								<div class="col-md-4 col-md-pull-4">
									<div class="num">
										<span class="icon"><i class="icon-phone"></i></span>
										<p><a href="#">Hotline</a><br><a href="#">(022) 4248 333</a></p>
									</div>
								</div>
								<div class="col-md-4">
									<div class="loc">
										<span class="icon"><i class="icon-location"></i></span>
										<p><a href="#">Jl. Kebonjati No.38 Bandung 40181 - Indonesia Telp. (022) 4248333 </a></p>
									</div>
								</div>
							</div>
						</div>
					</div>


				<!--  Head Menu -->
					<div class="col-xs-12 text-center">
						<div class="menu-1">
							<ul>
								<li><a href="<?php echo site_url('proses/');?>">Home</a></li>
								<li><a href="services.html">Services</a></li>
								<li class="has-dropdown">
									<a href="departments.html">Departments</a>
									<ul class="dropdown">
										<li><a href="departments-single.html">Plasetic Surgery Department</a></li>
										<li><a href="departments-single.html">Dental Department</a></li>
										<li><a href="departments-single.html">Psychological Department</a></li>
									</ul>
								</li>
								<li class="active">
									<a href="<?php echo site_url('reservasi');?>">Berobat</a>
								</li>
								<li><a href="<?php echo site_url('proses/contact_rs');?>">Contact</a></li>
								<li><a href="<?php echo site_url('proses/appoiment');?>"><span>Daftar</span></a></li>
							</ul>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</nav>
	<br>


			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<form action="#" class="appointment-wrap animate-box">
						
						<div class="row form-group">
							<div class="col-md-12">
								<label for="reka">Nomor Rekamedis</label>
								<input type="text" id="reka" class="form-control" placeholder="Masukkan No Rekamedis">
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="subject">Pilih Spesialis</label>


						        <select name="prop" id="prop"  class="form-control pilih "  required='' >
				                    <option>-- Silahkan pilih --</option>  
				                      <?php foreach($idk as $row2) { ?>
				                        <option  <?php if ($row2['id_spesialis']==$row2['id_spesialis']) ?> value="<?php echo $row2['id_spesialis'];?>"><?php echo $row2['jenis_spesialis']?></option> 
				                      <?php } ?>
				                  </select>
								<!-- <select name="jenis_spesialis" class="selectpicker form-control">
							 		<option>Choose Your Subject</option>
							 	</select> -->
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="subject">Pilih Dokter</label>
								<select id="kota"  name="nama_dokter" class="form-control pilih">
									<option value="">-- Silahkan pilih --</option>
									<?php foreach($idka as $row2) { ?>
				                        <option  <?php if ($row2['id_dokter']==$row2['id_dokter']) ?> value="<?php echo $row2['nama_dokter'];?>"><?php echo $row2['nama_dokter']?></option> 
				                      <?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<input type="submit" value="Kirim" class="btn btn-primary">
						</div>

						
					</form>
				</div>
			</div>
		</div>
		<br><br>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.min.js")?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.easing.1.3.js")?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url("assets/plugin/js/bootstrap.min.js")?>"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.waypoints.min.js")?>"></script>
	<!-- Stellar Parallax -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.stellar.min.js")?>"></script>
	<!-- Carousel -->
	<script src="<?php echo base_url("assets/plugin/js/owl.carousel.min.js")?>"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.flexslider-min.js")?>"></script>
	<!-- countTo -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.countTo.js")?>"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.magnific-popup.min.js")?>"></script>
	<script src="<?php echo base_url("assets/plugin/js/magnific-popup-options.js")?>"></script>
	<!-- Sticky Kit -->
	<script src="<?php echo base_url("assets/plugin/js/sticky-kit.min.js")?>"></script>
	<!-- Main -->
	<script src="<?php echo base_url("assets/plugin/js/main.js")?>"></script>

	</body>
</html>

