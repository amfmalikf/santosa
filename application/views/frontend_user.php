<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Santosa Hospital</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by QBOOTSTRAP.COM" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="QBOOTSTRAP.COM" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by QBOOTSTRAP.COM
		
	Website: 		http://qbootstrap.com/
	Email: 			info@qbootstrap.com
	Twitter: 		http://twitter.com/Q_bootstrap
	Facebook: 		https://www.facebook.com/Qbootstrap
	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/animate.css") ?>">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/icomoon.css")?>">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/bootstrap.css")?>">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/magnific-popup.css")?>">
	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/owl.carousel.min.css")?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/owl.theme.default.min.css")?>">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/flexslider.css")?>">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/style.css")?>">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url("assets/plugin/js/modernizr-2.6.2.min.js")?>"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>


		<!-- Info Head -->
	<div class="qbootstrap-loader"></div>
	
	<div id="page">
	<nav class="qbootstrap-nav" role="navigation">
		<div class="top-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="top">
							<div class="row">
								<div class="col-md-4 col-md-push-4 text-center">
									<div id="qbootstrap-logo"><a href="<?php echo site_url('proses/');?>"><i class="icon-plus-outline"></i>Santosa<span>care</span></a></div>
								</div>
								<div class="col-md-4 col-md-pull-4">
									<div class="num">
										<span class="icon"><i class="icon-phone"></i></span>
										<p><a href="#">Hotline</a><br><a href="#">(022) 4248 333</a></p>
									</div>
								</div>
								<div class="col-md-4">
									<div class="loc">
										<span class="icon"><i class="icon-location"></i></span>
										<p><a href="#">Jl. Kebonjati No.38 Bandung 40181 - Indonesia Telp. (022) 4248333 </a></p>
									</div>
								</div>
							</div>
						</div>
					</div>


<!--  Head Menu -->
					<div class="col-xs-20 text-center">
						<div class="menu-1">
							<ul>
								<li class="active"><a href="<?php echo site_url('proses/');?>">Home</a></li>
								<li><a href="services.html">Services</a></li>
								<li class="has-dropdown">
									<a href="departments.html">Departments</a>
									<ul class="dropdown">
										<li><a href="departments-single.html">Plasetic Surgery Department</a></li>
										<li><a href="departments-single.html">Dental Department</a></li>
										<li><a href="departments-single.html">Psychological Department</a></li>
									</ul>
								</li>
								<li class="has-dropdown">
									<a href="<?php echo site_url('reservasi');?>">Berobat</a>
								</li>
								<li><a href="<?php echo site_url('proses/contact_rs');?>">Contact</a></li>
								<li><a href="<?php echo site_url('daftar');?>"><span>Daftar</span></a></li>
							</ul>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</nav>
	



	<!-- Korsel -->
	<aside id="qbootstrap-hero">
		<div class="flexslider">
			<ul class="slides">
					<li style="background-image: url(<?php echo base_url("assets/plugin/images/gedung1.jpg);")?>">
		   		<div class="overlay"></div>
		   		<div class="container">
		   			<div class="row">
			   			<div class="col-md-8 col-md-offset-2 text-center slider-text">
			   				<div class="slider-text-inner">
			   					<h1>Rumah Sakit<br>Bertaraf<strong>  Internasional unggulan</strong> di Indonesia</h1>
									<h2>(To be the leading international hospital in Indonesia) </a></h2>
									<p>
									<a class="btn btn-primary btn-lg btn-learn" href="<?php echo site_url('proses/appoiment');?>">DAFTAR</a>
			   				</div>
			   			</div>
			   		</div>
		   		</div>
		   	</li>			
		  	</ul>
	  	</div>
	</aside>
	



<!-- Visi Misi TUjuan -->
	<div id="qbootstrap-services">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-10 animate-box">
					<div class="services">
						<span class="icon">
							<i class="icon-flow-merge"></i>
						</span>
						<div class="desc">
							<h3><a href="#">VISI</a></h3>
							<p>Menjadi rumah sakit bertaraf internasional unggulan di Indonesia</p>
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="row animate-box">
				<div class="col-md-12 col-md-offset-1">
					<div class="col-md-10 animate-box">
					<div class="services">
						<span class="icon">
							<i class="icon-flow-merge"></i>
						</span>
						<div class="desc">
							<h3><a href="#">MISI</a></h3>
							<p align="justify">1. Memberikan pelayanan medis dan keperawatan dengan standar professional setinggi mungkin yang mengacu pada peningkatan mutu pelayanan dan keselamatan pasien.</p>
							<p align="justify">2. Meningkatkan kualitas sumber daya manusia melalui pendidikan baik tenaga medis maupun non medis.</p>
							<p align="justify">3. Memberikan suasana pelayanan rumah sakit yang nyaman, ramah, efisien dan efektif.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="row animate-box">
				<div class="col-md-12 col-md-offset-1 qbootstrap-heading">
					<div class="col-md-10 animate-box">
					<div class="services">
						<span class="icon">
							<i class="icon-flow-merge"></i>
						</span>
						<div class="desc">
							<h3><a href="#">TUJUAN</a></h3>
							<p>1. Rumah Sakit Unggulan dalam pelayanan medis, khususnya bidang Jantung dan Saraf.</p>
							<p>2. Rumah Sakit Unggulan dalam bidang Service Excellence</p>
							<p>3. Rumah Sakit dengan tingkat okupansi dan utilisasi yang tinggi: BOR rata-rata 80% dan Kunjungan Rawat Jalan 1000 per hari dalam jangka waktu 5 tahun.</p>
							<p>4. Rumah Sakit memiliki sumber daya manusia yang kompeten melalui pendidikan dan pelatihan berkelanjutan.</p>
						</div>
					</div>
				</div>
			</div>
</div>
<hr>
				<div class="col-md-10 col-md-offset-1 ">


					<h2>Profil</h2>
					<p align="justify">Telah hadir di kota Bandung yang sejuk, sebuah rumah sakit swasta berkapasitas 400 tempat tidur berstandar internasional yang telah diresmikan oleh Menteri Kesehatan RI Dr. dr. Siti Fadilah Supari, Sp.JP (K) pada tanggal 4 November 2006, yaitu Santosa Hospital . Berdiri di atas lahan seluas 14,483 m 2 dengan total luas bangunan 38,727 m 2 , Santosa Hospital terdiri dari 9 lantai dan 2 basement untuk parkir. Dilengkapi dengan peralatan medis yang canggih dan didukung oleh lebih dari 200 dokter, diantaranya 60 dokter full time , tenaga medis dan paramedis yang terlatih dan profesional, menjadikan Santosa Hospital sebagai rumah sakit swasta terkemuka di Jawa Barat. Selain bekerja sama dengan Fakultas Kedokteran Universitas Padjajaran, Santosa Hospital juga menjalin kolaborasi dengan institusi/rumah sakit di luar negeri seperti Victoria Hearth Centre - Epworth Hospital Melbourne, Australia;</p><br>
					<p align="justify">
					SingHealth (Singapore General Hospital, National Neuroscience Institute, National Heart Centre of Singapore, National Cancer Centre, KK Women & Children Hospital Singapore, National Eye Centre); National Healthcare Group Singapore (National University Hospital, Tan Tock Seng Hospital) dan Parkway Healthcare Group (Mt. Elizabeth Hospital, Gleneagles Hospital dan East Shore Hospital). Hampir semua layanan spesialis/sub-spesialis tersedia di Santosa Hospital dengan layanan unggulan kami adalah Neuroscience Centre (Pusat Pengobatan Penyakit Saraf & Stroke), Cardiac Centre (Pusat Pengobatan Penyakit Jantung & Pembuluh Darah), Minimally Invasive Surgery (Bedah Laparoskopi) dan Skin Health & Beauty Centre . Fasilitas lainnya yang tersedia di Santosa Hospital yaitu studio apartment untuk keluarga pasien, healing garden (taman penyembuhan) dan helipad untuk evakuasi pasien melalui udara. Dengan menjiwai motto ‘ friendly and caring ', setiap staf Santosa Hospital akan melayani pasien dengan ramah, aman, nyaman dan dengan profesionalisme serta dedikasi yang tinggi. Santosa Hospital telah mendapatkan sertifikat 'Penuh Tingkat Lengkap'' dari Departemen Kesehatan RI dan sekarang telah terakreditasi internasional oleh JCI (Joint Commission International) yang berkantor di Amerika Serikat dan menjadi salah satu dari sedikit rumah sakit terbaik di dunia.</p><br>

					<p align="justify">
						Santosa Hospital Bandung Kopo<br><br>

						hadir di kota Bandung yang berhawa sejuk dan terletak di jalan K.H Wahid Hasyim (Kopo) No. 461 - 463. SHBK merupakan rumah sakit kedua Santosa di kota Bandung.<br><br>

						Santosa Hospital Bandung Kopo terdiri dari Gedung Utama (8 lantai & 1 basement) serta Gedung Pusat Pelayanan Kanker Terpadu (7 lantai) dan Gedung Jaminan Kesehatan Nasional (JKN)(6 lantai) yang dibangun diatas lahan seluas 1.7 Ha dengan luas bangunan 41.000 m2. Fasilitas penunjang yang tak kalah penting adalah area terbuka berkonsep Green Hospital serta area parkir luas.<br><br>

						SHBK adalah rumah sakit dengan fasilitas pelayanan kesehatan umum dengan layanan unggulan Onkologi/Kanker dan Trauma Centre, serta dilengkapi dengan peralatan mutakhir dan didukung oleh tenaga medis profesional.<br><br>

						Sebagai Rumah sakit yang memberikan jasa pelayanan profesional, SHBK memandang penting sumber daya manusia sebagai sumber daya utama dalam pelayanan kami. Oleh karena itu kami mempunyai komitmen kuat untuk selalu berusaha meningkatkan kemampuan dan profesionalisme melalui pendidikan berkesinambungan baik di dalam maupun di luar negeri.
					</p>

				</div>
			</div>
		</div>


	<footer id="qbootstrap-footer" role="contentinfo">
		<div class="row copyright">
			<div class="col-md-12 text-center">
				<p>
                    <small class="block">&copy; One Stop Health . Friendly And Caring.</small> 
                    <small class="block">Copyright © 2018 Santosa Hospital Bandung Central. All Rights Reserved</small>
                </p>
			</div>
		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.min.js")?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.easing.1.3.js")?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url("assets/plugin/js/bootstrap.min.js")?>"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.waypoints.min.js")?>"></script>
	<!-- Stellar Parallax -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.stellar.min.js")?>"></script>
	<!-- Carousel -->
	<script src="<?php echo base_url("assets/plugin/js/owl.carousel.min.js")?>"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.flexslider-min.js")?>"></script>
	<!-- countTo -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.countTo.js")?>"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.magnific-popup.min.js")?>"></script>
	<script src="<?php echo base_url("assets/plugin/js/magnific-popup-options.js")?>"></script>
	<!-- Sticky Kit -->
	<script src="<?php echo base_url("assets/plugin/js/sticky-kit.min.js")?>"></script>
	<!-- Main -->
	<script src="<?php echo base_url("assets/plugin/js/main.js")?>"></script>

	</body>
</html>

