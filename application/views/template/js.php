</div><!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">

    </div>
   <!--  <strong>IDNETSOLUTION &copy; 2017 </strong> All rights reserved. -->
</footer>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.3 -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
<script src='<?php echo base_url('assets/adminlte/js/select2.min.js') ?>'></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/slimScroll/jquery.slimScroll.min.js') ?>" type="text/javascript"></script>
<!-- FastClick -->
<script src='<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/fastclick/fastclick.min.js') ?>'></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/dist/js/app.min.js') ?>" type="text/javascript"></script>
<script src='<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.min.js') ?>'></script>

<script src='<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.min.js') ?>'></script>

<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jquery-bracket-master/dist/jquery.bracket.min.js')?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/parsley/parsley.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/parsley/i18n/id.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jQuery-Brackets-Bracket-World/dist/assets/scripts/jquery.bracket-world.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js')?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/timepicker/bootstrap-timepicker.min.js')?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/bower_components/morris.js/morris.min.js')?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.example1').DataTable();
	});

	$(function() {
    $( "#tgl" ).datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		endDate: 'y',
		// startDate: 'y',


		});
  	});

    $(function() {
    $( ".akhir" ).datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		});
  	});

  	$(function() {
    $( "#tgl2" ).datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		endDate: '-15y',


		});
  	});

	$(function() {
    $( "#thn" ).datepicker({
		autoclose: true,
		format: "yyyy",
    	viewMode: "years",
    	minViewMode: "years",
		startDate: '-7y',
		endDate: 'y'


		});
  	});

	$(function() {
    $( "#akhir_periode" ).datepicker({

		autoclose: true,
		format: "yyyy-mm-dd",
		startDate: '+70d',
		endDate: '+7m'


		});
  	});

	$(function() {
    $( "#akhir_pendaftaran" ).datepicker({

		autoclose: true,
		format: "yyyy-mm-dd",
		startDate: '+7d',
		endDate: '+2m'


		});
  	});

	$(".timepicker").timepicker({
      showInputs: false,
	  showMeridian :false,
	  defaultTime : false,
	  minuteStep : 5

    });

  $(".nonaktif").keydown(function(event) {
    return false;
});

function angka(a){
	var charCode = (a.which) ? a.which : event.keyCode
	if (charCode > 31 && (charCode <48 || charCode >57))
		return false;
	return true;
}


function limit_checkbox(max,identifier)
{
	var checkbox = $("input[name='"+identifier+"[]']");
	var checked  = $("input[name='"+identifier+"[]']:checked").length;
	checkbox.filter(':not(:checked)').prop('disabled', checked >= max);

}

 // New Code ~23/12/2017~
 $(document).ready(function () {
   $(".pilih").select2({
     placeholder: "Please Select"
   });
 });

 function angka(a) {
   if (!/^[0-9]+$/.test(a.value)) {
     a.value = a.value.substring(0,a.value.length-20);
   }
 }
</script>
