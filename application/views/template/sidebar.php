<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
      <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Admin<?php echo $this->session->userdata('namalengkap');?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form --><!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
            <li class="header">NAVIGASI</li>
            <li class="treeview">
                <a href="<?php echo site_url('dashboard');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <li class="treeview">
                <a href="<?php echo site_url('tb_penjadwalan');?>">
            <i class="fa fa-calendar"></i> <span>Penjadwalan Dokter</span></a></li>

             <li class="treeview">
                <a href="<?php echo site_url('tb_dokter');?>">
            <i class="fa fa-heartbeat"></i> <span>Daftar Dokter</span></a></li>

            <li class="treeview">
                <a href="<?php echo site_url('tb_spesialis');?>">
            <i class="fa fa-qrcode"></i> <span>Daftar Poliklinik</span></a></li>
           <!--  <li class="treeview">
                <a href="<?php echo site_url('transaksi');?>">
            <i class="fa fa-money"></i> <span>Transaksi Pelayanan</span></a></li>
 -->

            <li class="treeview">
                <a href="<?php echo site_url('login/logout');?>">
            <i class="fa fa-arrow-left"></i> <span>Logout</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">