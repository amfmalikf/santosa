<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Santosa Hospital</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
         <link href="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="<?php echo base_url('assets/adminlte/font-awesome-4.3.0/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
       
        <!-- Ionicons -->
        <link href="<?php echo base_url('assets/adminlte/ionicons-2.0.1/css/ionicons.min.css') ?>" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" type="text/css" />
        
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" type="text/css" />
        
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/dist/css/skins/_all-skins.min.css') ?>" rel="stylesheet" type="text/css" />
        
        
        <link href="<?php echo base_url('assets/adminlte/dropzone.min.css') ?>" rel="stylesheet" type="text/css" />
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/adminlte/basic.min.css') ?>">
        
        <script type="text/javascript" src="<?php echo base_url('assets/adminlte/jquery.js') ?>"></script>
        
        <link href="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jquery-bracket-master/dist/jquery.bracket.min.css')?>" rel="stylesheet">

        
<script type="text/javascript" src="<?php echo base_url('assets/adminlte/dropzone.min.js') ?>"></script>

<link href="<?php echo base_url('assets/parsley/parsley.css" rel="stylesheet'); ?>" type="text/css">

<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css')?>" type="text/css">

<link rel="stylesheet" href="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/timepicker/bootstrap-timepicker.min.css')?>" type="text/css">



<link href="<?php echo base_url('assets/adminlte/css/select2.min.css') ?>" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/bower_components/morris.js/morris.css')?> ">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/bower_components/jvectormap/jquery-jvectormap.css')?>">



        
        
        
        

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->