<?php 
// if($this->session->userdata('status') != "login"){
// 			redirect(base_url("index.php/auth"));
// }       
$this->load->view('template/head');
?>
<!--tambahkan custom css disini-->
<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>
<!-- Content Header (Page header) -->


<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        
        <div class="box-body">
           <?php 
  /*
   * Variabel $contentnya diambil dari libraries template.php
   * (application/libraries/template.php)
   * */
  echo $main; ?>
        </div><!-- /.box-body -->
        <div class="box-footer">
           
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->

<?php 
$this->load->view('template/js');
?>
<!--tambahkan custom js disini-->
<?php
$this->load->view('template/foot');
?>

<script type="text/javascript">
        
        $(function() {
        $( "#th" ).datepicker({
            autoclose: true,
            format: "yyyy-mm",
            viewMode: "years-months",
            
          });
        });
    </script>
