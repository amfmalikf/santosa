<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Santosa Hospital</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by QBOOTSTRAP.COM" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="QBOOTSTRAP.COM" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by QBOOTSTRAP.COM
		
	Website: 		http://qbootstrap.com/
	Email: 			info@qbootstrap.com
	Twitter: 		http://twitter.com/Q_bootstrap
	Facebook: 		https://www.facebook.com/Qbootstrap
	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/animate.css") ?>">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/icomoon.css")?>">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/bootstrap.css")?>">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/magnific-popup.css")?>">
	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/owl.carousel.min.css")?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/owl.theme.default.min.css")?>">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/flexslider.css")?>">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url("assets/plugin/css/style.css")?>">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url("assets/plugin/js/modernizr-2.6.2.min.js")?>"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>


		<!-- Info Head -->
	<div class="qbootstrap-loader"></div>
	
	<div id="page">
	<nav class="qbootstrap-nav" role="navigation">
		<div class="top-menu">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="top">
							<div class="row">
								<div class="col-md-4 col-md-push-4 text-center">
									<div id="qbootstrap-logo"><a href="<?php echo site_url('proses/');?>"><i class="icon-plus-outline"></i>Santosa<span>care 2</span></a></div>
								</div>
								<div class="col-md-4 col-md-pull-4">
									<div class="num">
										<span class="icon"><i class="icon-phone"></i></span>
										<p><a href="#">Hotline</a><br><a href="#">(022) 4248 333</a></p>
									</div>
								</div>
								<div class="col-md-4">
									<div class="loc">
										<span class="icon"><i class="icon-location"></i></span>
										<p><a href="#">Jl. Kebonjati No.38 Bandung 40181 - Indonesia Telp. (022) 4248333 </a></p>
									</div>
								</div>
							</div>
						</div>
					</div>


				<!--  Head Menu -->
					<div class="col-xs-12 text-center">
						<div class="menu-1">
							<ul>
								<li class="active"><a href="<?php echo site_url('proses/');?>">Home</a></li>
								<li><a href="services.html">Services</a></li>
								<li class="has-dropdown">
									<a href="departments.html">Departments</a>
									<ul class="dropdown">
										<li><a href="departments-single.html">Plasetic Surgery Department</a></li>
										<li><a href="departments-single.html">Dental Department</a></li>
										<li><a href="departments-single.html">Psychological Department</a></li>
									</ul>
								</li>
								<li class="has-dropdown">
									<a href="<?php echo site_url('reservasi');?>">Berobat</a>
								</li>
								<li><a href="<?php echo site_url('proses/contact_rs');?>">Contact</a></li>
								<li class="active"><a href="<?php echo site_url('daftar');?>"><span>Daftar</span></a></li>
							</ul>
						</div>
					</div>
				</div>
				
			</div>
		</div> 
	</nav>
	<br>


	<!-- konten -->
		<div class="container">
				<div class="col-md-10 col-md-offset-1 animate-box">
					<center><h3>Identitas Pemohon</h3></center>



<form action="<?php echo base_url('daftar/save') ?>" method="post">
						
			<!-- FORM 1  -->
						
						<div class="row form-group">
							<div class="col-md-3">
								<label for="fname">Nomer Identitas</label><br>
								<input type="radio" name="status_id" class="form-group" value="0"> KTP<BR>
								<input type="radio" name="status_id" class="form-group" value="1"> SIM
							</div>
							<div class="col-md-9">
							<label for="fname"></label><br>
							<input type="text" id="lname" name="no_id" class="form-control"  placeholder="masukan nomer identitas" maxlength="16">
							</div>
						</div>
						<hr>


			<!-- FORM 2 -->
						<div class="row form-group">
							<div class="col-md-6">
								<label for="fname">Nama Lengkap</label>
								<input type="text" name="namalengkap" id="fname" class="form-control" placeholder="Nama lengkap anda">
							</div>
							<div class="col-md-6">
								<label for="lname">Nama Panggilan</label>
								<input type="text" id="cname" name="namapanggilan" class="form-control" placeholder="Nama panggilan anda">
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6">
								<label for="email">Tempat Lahir</label>
								<input type="text" name="tmptlahir" id="tlahir" class="form-control" placeholder="Tempat lahir anda">
							</div>
							<div class="col-md-6">
								<label for="subject">Tanggal Lahir</label>
								<input type="date" id="subject" name="tgl_lahir" class="form-control" placeholder="Your subject of this message">
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6">
								<label for="email">Golongan Darah</label><br>
								<select name="gol_dar" class="form-control" required>
									<option value="">Silahkan pilih golongan darah anda</option>
									<option value="1">A</option>
									<option value="2">B</option>
									<option value="3">AB</option>
									<option value="4">O</option>
								</select>
							</div>
							<div class="col-md-6">
								<label for="subject">Jenis Kelamin</label><br>
								<center>
								<input type="radio" name="jk" value="1" > Laki - laki
								<input type="radio" name="jk" value="0" > Perempuan</center>
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6">
								<label for="agama">Agama</label><br>
								<select name="agama" class="form-control" required="">
									<option value="">Silahkan pilih agama</option>
									<option value="1">Islam</option>
									<option value="2">Katolik</option>
									<option value="3">Protestan</option>
									<option value="4">Hindu</option>
									<option value="4">Budha</option>
								</select>
							</div>
							<div class="col-md-6">
								<label for="subject">Status Diri</label><br>
								<select name="status_diri" class="form-control">
									<option value="">Silahkan pilih status diri</option>
									<option value="1">Menikah</option>
									<option value="2">Belum Menikah</option>
									<option value="3">Janda</option>
									<option value="4">Duda</option>
								</select>
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6">
								<label for="email">Kewarganegaraan</label><br>	
								<center>
								<input type="radio" name="kewarganegaraan" value="1"> WNI
								<input type="radio" name="kewarganegaraan" value="2"> WNA</center>

							</div>
							<div class="col-md-6">
								<label for="subject">Pendidikan</label>
								<br>
								<select name="pendidikan" class="form-control"><br>
									<option value="">Silahkan pilih pendidikan terakhir</option>
									<option value="1">SD</option>
									<option value="2">SMP / MTs</option>
									<option value="3">SMA / MA</option>
									<option value="4">D1</option>
									<option value="5">D2</option>
									<option value="6">D3</option>
									<option value="7">D4</option>
									<option value="8">S1</option>
									<option value="9">S2</option>
									<option value="10">S3</option>
								</select>
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6">
								<label for="alamat">Alamat</label>
								<input type="text" id="alamat" name="alamat" class="form-control" placeholder="Masukan alamat lengkap anda">
							</div>
							<div class="col-md-6">
								<label for="subject">Kelurahan</label>
								<input type="text" id="kelurahan" name="kelurahan" class="form-control" placeholder="Masukan kelurahan anda">
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6">
								<label for="email">Kecamatan</label>
								<input type="text" id="kecamatan" name="kecamatan" class="form-control" placeholder="Masukan kecamatan anda">
							</div>
							<div class="col-md-6">
								<label for="subject">Kabupaten</label>
								<input type="text" id="kabupaten" name="kabupaten" class="form-control" placeholder="Masukan kabupaten anda">
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6">
								<label for="kode_pos">Kode pos</label>
								<input type="text" id="kode_pos" name="kode_pos" class="form-control" placeholder="Masukan kode pos">
							</div>
							<div class="col-md-6">
								<label for="propinsi">Provinsi</label>
								<input type="text" id="provinsi" name="propinsi" class="form-control" placeholder="Masukan provinsi anda">
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6">
								<label for="no_rumah">No Telp Rumah</label>
								<input type="text" id="no_rumah" name="no_rumah" class="form-control" placeholder="Masukan nomor telpon rumah">
							</div>
							<div class="col-md-6">
								<label for="no_hp">Nomor Handphone</label>
								<input type="text" id="no_hp" name="no_hp" class="form-control" placeholder="Masukan handphone">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-6">
								<label for="pekerjaan" >Pekerjaan</label><br>
								<select name="pekerjaan" class="form-control" required="">
									<option value="">Silahkan pilih pekerjaan</option>
									<option value="1">Pelajar / Mahasiswa</option>
									<option value="2">PNS</option>
									<option value="3">TNI / POLRI</option>
									<option value="4">Swasta</option>
									<option value="5">Wiraswasta</option>
								</select>
							</div>
						</div>
					<hr>
<!-- FORM 3 -->

						<div class="row form-group">
							<div class="col-md-2">
								<label for="status_penjamin">Penjamin</label>
								<br>
								<input type="radio" name="status_penjamin" value="1"> Umum<br>
								<input type="radio" name="status_penjamin" value="2"> Perusahaan<br>
								<input type="radio" name="status_penjamin" value="3"> Asuransi<br>
							</div>
							<div class="col-md-8">
								<label for="subject"></label><br>
								<input type="text" id="subject" name="no_penjamin" class="form-control" placeholder="identitas penjamin anda">
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6">
								<label for="nama_penjamin">Nama Penjamin</label>
								<input type="text" id="nama_penjamin" name="nama_penjamin" class="form-control" placeholder="Masukan nama penjamin anda">
							</div>
							<div class="col-md-6">
								<label for="hubungan_penjamin">Hubungan Penjamin</label>
								<input type="text" id="hubungan_penjamin" name="hubungan_penjamin" class="form-control" placeholder="Masukan status hubungan penjamin anda">
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-6">
								<label for="nama_penjamin">Alamat Penjamin</label>
								<input type="text" id="nama_penjamin" name="nama_penjamin" class="form-control" placeholder="Masukan nama penjamin anda">
							</div>
							<div class="col-md-6">
								<label for="notelp_penjamin">Nomor Telpon </label>
								<input type="text" id="notelp_penjamin" name="notelp_penjamin" class="form-control" placeholder="Masukan nomor telp penjamin anda">
							</div>
						</div>

						<div class="form-group text-center">
							<input type="submit" class="btn btn-primary">
						</div>

					</form>		
				</div>
			</div>		
		</div>
 
		
		<br><br>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.min.js")?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.easing.1.3.js")?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url("assets/plugin/js/bootstrap.min.js")?>"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.waypoints.min.js")?>"></script>
	<!-- Stellar Parallax -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.stellar.min.js")?>"></script>
	<!-- Carousel -->
	<script src="<?php echo base_url("assets/plugin/js/owl.carousel.min.js")?>"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.flexslider-min.js")?>"></script>
	<!-- countTo -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.countTo.js")?>"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url("assets/plugin/js/jquery.magnific-popup.min.js")?>"></script>
	<script src="<?php echo base_url("assets/plugin/js/magnific-popup-options.js")?>"></script>
	<!-- Sticky Kit -->
	<script src="<?php echo base_url("assets/plugin/js/sticky-kit.min.js")?>"></script>
	<!-- Main -->
	<script src="<?php echo base_url("assets/plugin/js/main.js")?>"></script>

	</body>
</html>

