<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_penjadwalan
 * @created on : Friday, 17-Aug-2018 16:03:16
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class tb_penjadwalans extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tb_penjadwalan
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all() 
    {

        $this->db->select('*');
        $this->db->from('tb_penjadwalan a');
        $this->db->join('tb_dokter b','a.id_dokter=b.id_dokter');
        $this->db->join('tb_spesialis c','a.id_spesialis=c.id_spesialis');
        $query = $this->db->get();
        return $query->result_array();
    }

    

    /**
     *  Count All tb_penjadwalan
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tb_penjadwalan');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tb_penjadwalan
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
                
        $this->db->like('nama_dokter', $keyword);  
                
        $this->db->like('jdwl_praktek', $keyword);  
                
        $this->db->like('jam_praktek', $keyword);  
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tb_penjadwalan');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tb_penjadwalan
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tb_penjadwalan');        
                
        $this->db->like('nama_dokter', $keyword);  
                
        $this->db->like('jdwl_praktek', $keyword);  
                
        $this->db->like('jam_praktek', $keyword);  
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tb_penjadwalan
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_penjadwalan', $id);
        $result = $this->db->get('tb_penjadwalan');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tb_penjadwalan
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'id_poli' => '',
            
                'nama_dokter' => '',
            
                'jdwl_praktek' => '',
            
                'jam_praktek' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'id_spesialis' => strip_tags($this->input->post('id_spesialis', TRUE)),
        
            'id_dokter' => strip_tags($this->input->post('id_dokter', TRUE)),
        
            'jdwl_praktek' => strip_tags($this->input->post('jdwl_praktek', TRUE)),
        
            'jam_praktek' => strip_tags($this->input->post('jam_praktek', TRUE)),
        
        );
        
        
        $this->db->insert('tb_penjadwalan', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'id_poli' => strip_tags($this->input->post('id_poli', TRUE)),
        
                'nama_dokter' => strip_tags($this->input->post('nama_dokter', TRUE)),
        
                'jdwl_praktek' => strip_tags($this->input->post('jdwl_praktek', TRUE)),
        
                'jam_praktek' => strip_tags($this->input->post('jam_praktek', TRUE)),
        
        );
        
        
        $this->db->where('id_penjadwalan', $id);
        $this->db->update('tb_penjadwalan', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_penjadwalan', $id);
        $this->db->delete('tb_penjadwalan');
        
    }




 public function sms_get_spesialis() 
    {

        $result = $this->db->get('tb_spesialis');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

      public function sms_get_dokter() 
    {

        $result = $this->db->get('tb_dokter');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }


    



}
