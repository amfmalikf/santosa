<div class="row">
	<div class="col-lg-12 col-md-12">		
		<?php 
                
                echo create_breadcrumb();		
                echo $this->session->flashdata('notify');
                
                ?>
	</div>
</div><!-- /.row -->

<?php echo form_open(site_url('tb_penjadwalan/' . $action),'role="form" class="form-horizontal" id="form_tb_penjadwalan" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-signal"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="id_poli" class="col-sm-2 control-label">Pilih Spesialis<span class="required-input"></span></label>
                <div class="col-sm-6">                                   
                        <select name="id_spesialis" class="form-control pilih">
                  <option value="">-- Silahkan pilih --</option>
                  <?php foreach($rev as $row2) { ?>
                                <option  <?php if ($row2['id_spesialis']==$row2['id_spesialis']) ?> value="<?php echo $row2['id_spesialis'];?>"><?php echo $row2['jenis_spesialis']?></option> 
                              <?php } ?>
                </select>          </div>
              </div> <!--/ Id Poli -->
                          
               <div class="form-group">
                   <label for="nama_dokter" class="col-sm-2 control-label">Nama Dokter <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                 <select name="id_dokter" class="form-control pilih">
                  <option value="">-- Silahkan pilih --</option>
                  <?php foreach($dok as $row2) { ?>
                                <option  <?php if ($row2['id_dokter']==$row2['id_dokter']) ?> value="<?php echo $row2['id_dokter'];?>"><?php echo $row2['nama_dokter']?></option> 
                              <?php } ?>
                </select>
                </div>
              </div> <!--/ Nama Dokter -->
                                                 
               <div class="form-group">
                   <label for="jdwl_praktek" class="col-sm-2 control-label">Jadwal Praktek</label>
                <div class="col-sm-6">                                   
                  <input type="text" name="jdwl_praktek" class="form-control">
                </div>
              </div> <!--/ Jdwl Praktek -->
                          
               <div class="form-group">
                   <label for="jam_praktek" class="col-sm-2 control-label">Jam Praktek</label>
                <div class="col-sm-6">                                   
                  <input type="text" name="jam_praktek" class="form-control">
                </div>
              </div> <!--/ Jam Praktek -->

           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0">
                   <a href="<?php echo site_url('tb_penjadwalan'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  