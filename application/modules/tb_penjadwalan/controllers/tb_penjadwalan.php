<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tb_penjadwalan
 * @created on : Friday, 17-Aug-2018 16:03:16
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class tb_penjadwalan extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('tb_penjadwalans');
    }
    

    /**
    * List all data tb_penjadwalan
    *
    */
    public function index() 
    {
        $config = array(
            'base_url'          => site_url('tb_penjadwalan/index/'),
            'total_rows'        => $this->tb_penjadwalans->count_all(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
            
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['pagination']     = $this->pagination->create_links();
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['tb_penjadwalans']       = $this->tb_penjadwalans->get_all($config['per_page'], $this->uri->segment(3));
        $this->template->render('tb_penjadwalan/view',$data);
	      
    }

    

    /**
    * Call Form to Add  New tb_penjadwalan
    *
    */
    public function add() 
    {       

        $data['tb_penjadwalan'] = $this->tb_penjadwalans->add();
        $data['action']  = 'tb_penjadwalan/save';
        $data['rev']=$this->tb_penjadwalans->sms_get_spesialis();
        $data['dok']=$this->tb_penjadwalans->sms_get_dokter();
     
        $this->template->js_add('
                $(document).ready(function(){
                // binds form submission and fields to the validation engine
                $("#form_tb_penjadwalan").parsley();
                        });','embed');
      
        $this->template->render('tb_penjadwalan/form',$data);

    }

    

    /**
    * Call Form to Modify tb_penjadwalan
    *
    */
    public function edit($id='') 
    {
        if ($id != '') 
        {

            $data['tb_penjadwalan']      = $this->tb_penjadwalans->get_one($id);
            $data['action']       = 'tb_penjadwalan/save/' . $id;           
      
            $this->template->js_add('
                     $(document).ready(function(){
                    // binds form submission and fields to the validation engine
                    $("#form_tb_penjadwalan").parsley();
                                    });','embed');
            
            $this->template->render('tb_penjadwalan/form',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_penjadwalan'));
        }
    }


    
    /**
    * Save & Update data  tb_penjadwalan
    *
    */
    public function save($id =NULL) 
    {
        // validation config
        $config = array(
                  
                    array(
                        'field' => 'id_poli',
                        'label' => 'Id Poli',
                        'rules' => 'trim|xss_clean'
                        ),
                    
                    array(
                        'field' => 'nama_dokter',
                        'label' => 'Nama Dokter',
                        'rules' => 'trim|xss_clean'
                        ),
                    
                    array(
                        'field' => 'jdwl_praktek',
                        'label' => 'Jdwl Praktek',
                        'rules' => 'trim|xss_clean'
                        ),
                    
                    array(
                        'field' => 'jam_praktek',
                        'label' => 'Jam Praktek',
                        'rules' => 'trim|xss_clean'
                        ),
                               
                  );
            
        // if id NULL then add new data
        if(!$id)
        {    
                  $this->form_validation->set_rules($config);

                  if ($this->form_validation->run() == TRUE) 
                  {
                      if ($this->input->post()) 
                      {
                          
                          $this->tb_penjadwalans->save();
                          $this->session->set_flashdata('notif', notify('Data berhasil di simpan','success'));
                          redirect('tb_penjadwalan');
                      }
                  } 
                  else // If validation incorrect 
                  {
                      $this->add();
                  }
         }
         else // Update data if Form Edit send Post and ID available
         {               
                $this->form_validation->set_rules($config);

                if ($this->form_validation->run() == TRUE) 
                {
                    if ($this->input->post()) 
                    {
                        $this->tb_penjadwalans->update($id);
                        $this->session->set_flashdata('notif', notify('Data berhasil di update','success'));
                        redirect('tb_penjadwalan');
                    }
                } 
                else // If validation incorrect 
                {
                    $this->edit($id);
                }
         }
    }

    
    
    /**
    * Detail tb_penjadwalan
    *
    */
    public function show($id='') 
    {
        if ($id != '') 
        {

            $data['tb_penjadwalan'] = $this->tb_penjadwalans->get_one($id);            
            $this->template->render('tb_penjadwalan/_show',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_penjadwalan'));
        }
    }
    
    
    /**
    * Search tb_penjadwalan like ""
    *
    */   
    public function search()
    {
        if($this->input->post('q'))
        {
            $keyword = $this->input->post('q');
            
            $this->session->set_userdata(
                        array('keyword' => $this->input->post('q',TRUE))
                    );
        }
        
         $config = array(
            'base_url'          => site_url('tb_penjadwalan/search/'),
            'total_rows'        => $this->tb_penjadwalans->count_all_search(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['pagination']     = $this->pagination->create_links();
        $data['tb_penjadwalans']       = $this->tb_penjadwalans->get_search($config['per_page'], $this->uri->segment(3));
       
        $this->template->render('tb_penjadwalan/view',$data);
    }
    
    
    /**
    * Delete tb_penjadwalan by ID
    *
    */
    public function destroy($id) 
    {        
        if ($id) 
        {
            $this->tb_penjadwalans->destroy($id);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('tb_penjadwalan');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('tb_penjadwalan');
        }       
    }





}

?>
