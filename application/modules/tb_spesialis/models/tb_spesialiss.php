<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_spesialis
 * @created on : Thursday, 02-Aug-2018 13:47:08
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class tb_spesialiss extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tb_spesialis
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all($limit, $offset) 
    {

        $result = $this->db->get('tb_spesialis', $limit, $offset);

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    

    /**
     *  Count All tb_spesialis
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tb_spesialis');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tb_spesialis
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
                
        $this->db->like('jenis_spesialis', $keyword);  
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tb_spesialis');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tb_spesialis
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tb_spesialis');        
                
        $this->db->like('jenis_spesialis', $keyword);  
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tb_spesialis
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_spesialis', $id);
        $result = $this->db->get('tb_spesialis');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tb_spesialis
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'jenis_spesialis' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'jenis_spesialis' => strip_tags($this->input->post('jenis_spesialis', TRUE)),
        
        );
        
        
        $this->db->insert('tb_spesialis', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'jenis_spesialis' => strip_tags($this->input->post('jenis_spesialis', TRUE)),
        
        );
        
        
        $this->db->where('id_spesialis', $id);
        $this->db->update('tb_spesialis', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_spesialis', $id);
        $this->db->delete('tb_spesialis');
        
    }



public function sms_get_spesialis() 
    {

        $result = $this->db->get('tb_spesialis');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }



    



}
