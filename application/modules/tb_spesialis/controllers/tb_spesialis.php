<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tb_spesialis
 * @created on : Thursday, 02-Aug-2018 13:47:08
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class tb_spesialis extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('tb_spesialiss');
    }
    

    /**
    * List all data tb_spesialis
    *
    */
    public function index() 
    {
        $config = array(
            'base_url'          => site_url('tb_spesialis/index/'),
            'total_rows'        => $this->tb_spesialiss->count_all(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
            
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['pagination']     = $this->pagination->create_links();
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['tb_spesialiss']       = $this->tb_spesialiss->get_all($config['per_page'], $this->uri->segment(3));
        $this->template->render('tb_spesialis/view',$data);
	      
    }

    

    /**
    * Call Form to Add  New tb_spesialis
    *
    */
    public function add() 
    {       
        $data['tb_spesialis'] = $this->tb_spesialiss->add();
        $data['action']  = 'tb_spesialis/save';
        
     
        $this->template->js_add('
                $(document).ready(function(){
                // binds form submission and fields to the validation engine
                $("#form_tb_spesialis").parsley();
                        });','embed');
      
        $this->template->render('tb_spesialis/form',$data);

    }

    

    /**
    * Call Form to Modify tb_spesialis
    *
    */
    public function edit($id='') 
    {
        if ($id != '') 
        {

            $data['tb_spesialis']      = $this->tb_spesialiss->get_one($id);
            $data['action']       = 'tb_spesialis/save/' . $id;           
      
            $this->template->js_add('
                     $(document).ready(function(){
                    // binds form submission and fields to the validation engine
                    $("#form_tb_spesialis").parsley();
                                    });','embed');
            
            $this->template->render('tb_spesialis/form',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_spesialis'));
        }
    }


    
    /**
    * Save & Update data  tb_spesialis
    *
    */
    public function save($id =NULL) 
    {
        // validation config
        $config = array(
                  
                    array(
                        'field' => 'jenis_spesialis',
                        'label' => 'Jenis Spesialis',
                        'rules' => 'trim|xss_clean'
                        ),
                               
                  );
            
        // if id NULL then add new data
        if(!$id)
        {    
                  $this->form_validation->set_rules($config);

                  if ($this->form_validation->run() == TRUE) 
                  {
                      if ($this->input->post()) 
                      {
                          
                          $this->tb_spesialiss->save();
                          $this->session->set_flashdata('notif', notify('Data berhasil di simpan','success'));
                          redirect('tb_spesialis');
                      }
                  } 
                  else // If validation incorrect 
                  {
                      $this->add();
                  }
         }
         else // Update data if Form Edit send Post and ID available
         {               
                $this->form_validation->set_rules($config);

                if ($this->form_validation->run() == TRUE) 
                {
                    if ($this->input->post()) 
                    {
                        $this->tb_spesialiss->update($id);
                        $this->session->set_flashdata('notif', notify('Data berhasil di update','success'));
                        redirect('tb_spesialis');
                    }
                } 
                else // If validation incorrect 
                {
                    $this->edit($id);
                }
         }
    }

    
    
    /**
    * Detail tb_spesialis
    *
    */
    public function show($id='') 
    {
        if ($id != '') 
        {

            $data['tb_spesialis'] = $this->tb_spesialiss->get_one($id);            
            $this->template->render('tb_spesialis/_show',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_spesialis'));
        }
    }
    
    
    /**
    * Search tb_spesialis like ""
    *
    */   
    public function search()
    {
        if($this->input->post('q'))
        {
            $keyword = $this->input->post('q');
            
            $this->session->set_userdata(
                        array('keyword' => $this->input->post('q',TRUE))
                    );
        }
        
         $config = array(
            'base_url'          => site_url('tb_spesialis/search/'),
            'total_rows'        => $this->tb_spesialiss->count_all_search(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['pagination']     = $this->pagination->create_links();
        $data['tb_spesialiss']       = $this->tb_spesialiss->get_search($config['per_page'], $this->uri->segment(3));
       
        $this->template->render('tb_spesialis/view',$data);
    }
    
    
    /**
    * Delete tb_spesialis by ID
    *
    */
    public function destroy($id) 
    {        
        if ($id) 
        {
            $this->tb_spesialiss->destroy($id);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('tb_spesialis');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('tb_spesialis');
        }       
    }

}

?>
