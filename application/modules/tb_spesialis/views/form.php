<div class="row">
	<div class="col-lg-12 col-md-12">		
		<?php 
                
                echo create_breadcrumb();		
                echo $this->session->flashdata('notify');
                
                ?>
	</div>
</div><!-- /.row -->

<?php echo form_open(site_url('tb_spesialis/' . $action),'role="form" class="form-horizontal" id="form_tb_spesialis" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-signal"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="jenis_spesialis" class="col-sm-2 control-label">Jenis Spesialis</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'jenis_spesialis',
                                 'id'           => 'jenis_spesialis',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Jenis Spesialis',
                                 
                                 ),
                                 set_value('jenis_spesialis',$tb_spesialis['jenis_spesialis'])
                           );             
                  ?>
                 <?php echo form_error('jenis_spesialis');?>
                </div>
              </div> <!--/ Jenis Spesialis -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0">
                   <a href="<?php echo site_url('tb_spesialis'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  