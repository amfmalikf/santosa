<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tb_pendaftaran
 * @created on : Monday, 09-Jul-2018 14:13:07
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class tb_pendaftaran extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('tb_pendaftarans');
    }
    

    /**
    * List all data tb_pendaftaran
    *
    */
    public function index() 
    {
        $config = array(
            'base_url'          => site_url('tb_pendaftaran/index/'),
            'total_rows'        => $this->tb_pendaftarans->count_all(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
            
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['pagination']     = $this->pagination->create_links();
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['tb_pendaftarans']       = $this->tb_pendaftarans->get_all($config['per_page'], $this->uri->segment(3));
        $this->template->render('tb_pendaftaran/view',$data);
	      
    }

    

    /**
    * Call Form to Add  New tb_pendaftaran
    *
    */
    public function add() 
    {       
        $data['tb_pendaftaran'] = $this->tb_pendaftarans->add();
        $data['action']  = 'tb_pendaftaran/save';
     
        $this->template->js_add('
                $(document).ready(function(){
                // binds form submission and fields to the validation engine
                $("#form_tb_pendaftaran").parsley();
                        });','embed');
      
        $this->template->render('tb_pendaftaran/form',$data);

    }

    

    /**
    * Call Form to Modify tb_pendaftaran
    *
    */
    public function edit($id='') 
    {
        if ($id != '') 
        {

            $data['tb_pendaftaran']      = $this->tb_pendaftarans->get_one($id);
            $data['action']       = 'tb_pendaftaran/save/' . $id;           
      
            $this->template->js_add('
                     $(document).ready(function(){
                    // binds form submission and fields to the validation engine
                    $("#form_tb_pendaftaran").parsley();
                                    });','embed');
            
            $this->template->render('tb_pendaftaran/form',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_pendaftaran'));
        }
    }


    
    /**
    * Save & Update data  tb_pendaftaran
    *
    */
    public function save($id =NULL) 
    {
        // validation config
        $config = array(
                  
                    array(
                        'field' => 'status_id',
                        'label' => 'Status',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'namalengkap',
                        'label' => 'Namalengkap',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'namapanggilan',
                        'label' => 'Namapanggilan',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'tmptlahir',
                        'label' => 'Tmptlahir',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'tgl_lahir',
                        'label' => 'Tgl Lahir',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'gol_dar',
                        'label' => 'Gol Dar',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'jk',
                        'label' => 'Jk',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'agama',
                        'label' => 'Agama',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'status_diri',
                        'label' => 'Status Diri',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'kewarganegaraan',
                        'label' => 'Kewarganegaraan',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'alamat',
                        'label' => 'Alamat',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'kelurahan',
                        'label' => 'Kelurahan',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'kecamatan',
                        'label' => 'Kecamatan',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'kabupaten',
                        'label' => 'Kabupaten',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'kode_pos',
                        'label' => 'Kode Pos',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'propinsi',
                        'label' => 'Propinsi',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'no_rumah',
                        'label' => 'No Rumah',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'no_hp',
                        'label' => 'No Hp',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'pekerjaan',
                        'label' => 'Pekerjaan',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'status_penjamin',
                        'label' => 'Status Penjamin',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'nama_penjamin',
                        'label' => 'Nama Penjamin',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'hubungan_penjamin',
                        'label' => 'Hubungan Penjamin',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'alamat_penjamin',
                        'label' => 'Alamat Penjamin',
                        'rules' => 'trim|xss_clean|required'
                        ),
                    
                    array(
                        'field' => 'notelp_penjamin',
                        'label' => 'Notelp Penjamin',
                        'rules' => 'trim|xss_clean|required'
                        ),
                               
                  );
            
        // if id NULL then add new data
        if(!$id)
        {    
                  $this->form_validation->set_rules($config);

                  if ($this->form_validation->run() == TRUE) 
                  {
                      if ($this->input->post()) 
                      {
                          
                          $this->tb_pendaftarans->save();
                          $this->session->set_flashdata('notif', notify('Data berhasil di simpan','success'));
                          redirect('tb_pendaftaran');
                      }
                  } 
                  else // If validation incorrect 
                  {
                      $this->add();
                  }
         }
         else // Update data if Form Edit send Post and ID available
         {               
                $this->form_validation->set_rules($config);

                if ($this->form_validation->run() == TRUE) 
                {
                    if ($this->input->post()) 
                    {
                        $this->tb_pendaftarans->update($id);
                        $this->session->set_flashdata('notif', notify('Data berhasil di update','success'));
                        redirect('tb_pendaftaran');
                    }
                } 
                else // If validation incorrect 
                {
                    $this->edit($id);
                }
         }
    }

    
    
    /**
    * Detail tb_pendaftaran
    *
    */
    public function show($id='') 
    {
        if ($id != '') 
        {

            $data['tb_pendaftaran'] = $this->tb_pendaftarans->get_one($id);            
            $this->template->render('tb_pendaftaran/_show',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_pendaftaran'));
        }
    }
    
    
    /**
    * Search tb_pendaftaran like ""
    *
    */   
    public function search()
    {
        if($this->input->post('q'))
        {
            $keyword = $this->input->post('q');
            
            $this->session->set_userdata(
                        array('keyword' => $this->input->post('q',TRUE))
                    );
        }
        
         $config = array(
            'base_url'          => site_url('tb_pendaftaran/search/'),
            'total_rows'        => $this->tb_pendaftarans->count_all_search(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['pagination']     = $this->pagination->create_links();
        $data['tb_pendaftarans']       = $this->tb_pendaftarans->get_search($config['per_page'], $this->uri->segment(3));
       
        $this->template->render('tb_pendaftaran/view',$data);
    }
    
    
    /**
    * Delete tb_pendaftaran by ID
    *
    */
    public function destroy($id) 
    {        
        if ($id) 
        {
            $this->tb_pendaftarans->destroy($id);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('tb_pendaftaran');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('tb_pendaftaran');
        }       
    }

}

?>
