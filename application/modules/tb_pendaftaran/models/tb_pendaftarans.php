<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_pendaftaran
 * @created on : Monday, 09-Jul-2018 14:13:08
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class tb_pendaftarans extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tb_pendaftaran
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all($limit, $offset) 
    {

        $result = $this->db->get('tb_pendaftaran', $limit, $offset);

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    

    /**
     *  Count All tb_pendaftaran
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tb_pendaftaran');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tb_pendaftaran
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
                
        $this->db->like('status_id', $keyword);  
                
        $this->db->like('namalengkap', $keyword);  
                
        $this->db->like('namapanggilan', $keyword);  
                
        $this->db->like('tmptlahir', $keyword);  
                
        $this->db->like('alamat', $keyword);  
                
        $this->db->like('kelurahan', $keyword);  
                
        $this->db->like('kecamatan', $keyword);  
                
        $this->db->like('kabupaten', $keyword);  
                
        $this->db->like('propinsi', $keyword);  
                
        $this->db->like('nama_penjamin', $keyword);  
                
        $this->db->like('hubungan_penjamin', $keyword);  
                
        $this->db->like('alamat_penjamin', $keyword);  
                
        $this->db->like('notelp_penjamin', $keyword);  
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tb_pendaftaran');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tb_pendaftaran
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tb_pendaftaran');        
                
        $this->db->like('status_id', $keyword);  
                
        $this->db->like('namalengkap', $keyword);  
                
        $this->db->like('namapanggilan', $keyword);  
                
        $this->db->like('tmptlahir', $keyword);  
                
        $this->db->like('alamat', $keyword);  
                
        $this->db->like('kelurahan', $keyword);  
                
        $this->db->like('kecamatan', $keyword);  
                
        $this->db->like('kabupaten', $keyword);  
                
        $this->db->like('propinsi', $keyword);  
                
        $this->db->like('nama_penjamin', $keyword);  
                
        $this->db->like('hubungan_penjamin', $keyword);  
                
        $this->db->like('alamat_penjamin', $keyword);  
                
        $this->db->like('notelp_penjamin', $keyword);  
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tb_pendaftaran
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_pendaftaran', $id);
        $result = $this->db->get('tb_pendaftaran');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tb_pendaftaran
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'status_id' => '',
            
                'namalengkap' => '',
            
                'namapanggilan' => '',
            
                'tmptlahir' => '',
            
                'tgl_lahir' => '',
            
                'gol_dar' => '',
            
                'jk' => '',
            
                'agama' => '',
            
                'status_diri' => '',
            
                'kewarganegaraan' => '',
            
                'alamat' => '',
            
                'kelurahan' => '',
            
                'kecamatan' => '',
            
                'kabupaten' => '',
            
                'kode_pos' => '',
            
                'propinsi' => '',
            
                'no_rumah' => '',
            
                'no_hp' => '',
            
                'pekerjaan' => '',
            
                'status_penjamin' => '',
            
                'nama_penjamin' => '',
            
                'hubungan_penjamin' => '',
            
                'alamat_penjamin' => '',
            
                'notelp_penjamin' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'status_id' => strip_tags($this->input->post('status_id', TRUE)),
        
            'namalengkap' => strip_tags($this->input->post('namalengkap', TRUE)),
        
            'namapanggilan' => strip_tags($this->input->post('namapanggilan', TRUE)),
        
            'tmptlahir' => strip_tags($this->input->post('tmptlahir', TRUE)),
        
            'tgl_lahir' => strip_tags($this->input->post('tgl_lahir', TRUE)),
        
            'gol_dar' => strip_tags($this->input->post('gol_dar', TRUE)),
        
            'jk' => strip_tags($this->input->post('jk', TRUE)),
        
            'agama' => strip_tags($this->input->post('agama', TRUE)),
        
            'status_diri' => strip_tags($this->input->post('status_diri', TRUE)),
        
            'kewarganegaraan' => strip_tags($this->input->post('kewarganegaraan', TRUE)),
        
            'alamat' => strip_tags($this->input->post('alamat', TRUE)),
        
            'kelurahan' => strip_tags($this->input->post('kelurahan', TRUE)),
        
            'kecamatan' => strip_tags($this->input->post('kecamatan', TRUE)),
        
            'kabupaten' => strip_tags($this->input->post('kabupaten', TRUE)),
        
            'kode_pos' => strip_tags($this->input->post('kode_pos', TRUE)),
        
            'propinsi' => strip_tags($this->input->post('propinsi', TRUE)),
        
            'no_rumah' => strip_tags($this->input->post('no_rumah', TRUE)),
        
            'no_hp' => strip_tags($this->input->post('no_hp', TRUE)),
        
            'pekerjaan' => strip_tags($this->input->post('pekerjaan', TRUE)),
        
            'status_penjamin' => strip_tags($this->input->post('status_penjamin', TRUE)),
        
            'nama_penjamin' => strip_tags($this->input->post('nama_penjamin', TRUE)),
        
            'hubungan_penjamin' => strip_tags($this->input->post('hubungan_penjamin', TRUE)),
        
            'alamat_penjamin' => strip_tags($this->input->post('alamat_penjamin', TRUE)),
        
            'notelp_penjamin' => strip_tags($this->input->post('notelp_penjamin', TRUE)),
        
        );
        
        
        $this->db->insert('tb_pendaftaran', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'status_id' => strip_tags($this->input->post('status_id', TRUE)),
        
                'namalengkap' => strip_tags($this->input->post('namalengkap', TRUE)),
        
                'namapanggilan' => strip_tags($this->input->post('namapanggilan', TRUE)),
        
                'tmptlahir' => strip_tags($this->input->post('tmptlahir', TRUE)),
        
                'tgl_lahir' => strip_tags($this->input->post('tgl_lahir', TRUE)),
        
                'gol_dar' => strip_tags($this->input->post('gol_dar', TRUE)),
        
                'jk' => strip_tags($this->input->post('jk', TRUE)),
        
                'agama' => strip_tags($this->input->post('agama', TRUE)),
        
                'status_diri' => strip_tags($this->input->post('status_diri', TRUE)),
        
                'kewarganegaraan' => strip_tags($this->input->post('kewarganegaraan', TRUE)),
        
                'alamat' => strip_tags($this->input->post('alamat', TRUE)),
        
                'kelurahan' => strip_tags($this->input->post('kelurahan', TRUE)),
        
                'kecamatan' => strip_tags($this->input->post('kecamatan', TRUE)),
        
                'kabupaten' => strip_tags($this->input->post('kabupaten', TRUE)),
        
                'kode_pos' => strip_tags($this->input->post('kode_pos', TRUE)),
        
                'propinsi' => strip_tags($this->input->post('propinsi', TRUE)),
        
                'no_rumah' => strip_tags($this->input->post('no_rumah', TRUE)),
        
                'no_hp' => strip_tags($this->input->post('no_hp', TRUE)),
        
                'pekerjaan' => strip_tags($this->input->post('pekerjaan', TRUE)),
        
                'status_penjamin' => strip_tags($this->input->post('status_penjamin', TRUE)),
        
                'nama_penjamin' => strip_tags($this->input->post('nama_penjamin', TRUE)),
        
                'hubungan_penjamin' => strip_tags($this->input->post('hubungan_penjamin', TRUE)),
        
                'alamat_penjamin' => strip_tags($this->input->post('alamat_penjamin', TRUE)),
        
                'notelp_penjamin' => strip_tags($this->input->post('notelp_penjamin', TRUE)),
        
        );
        
        
        $this->db->where('id_pendaftaran', $id);
        $this->db->update('tb_pendaftaran', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_pendaftaran', $id);
        $this->db->delete('tb_pendaftaran');
        
    }







    



}
