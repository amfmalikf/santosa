<div class="row">
	<div class="col-lg-12 col-md-12">		
		<?php 
                
                echo create_breadcrumb();		
                echo $this->session->flashdata('notify');
                
                ?>
	</div>
</div><!-- /.row -->

<section class="panel panel-default">
    <header class="panel-heading">
        <div class="row">
            <div class="col-md-8 col-xs-3">                
                <?php
                                  echo anchor(
                                           site_url('tb_pendaftaran/add'),
                                            '<i class="glyphicon glyphicon-plus"></i>',
                                            'class="btn btn-success btn-sm" data-tooltip="tooltip" data-placement="top" title="Tambah Data"'
                                          );
                 ?>
                
            </div>
            <div class="col-md-4 col-xs-9">
                                           
                 <?php echo form_open(site_url('tb_pendaftaran/search'), 'role="search" class="form"') ;?>       
                           <div class="input-group pull-right">                      
                                 <input type="text" class="form-control input-sm" placeholder="Cari data" name="q" autocomplete="off"> 
                                 <span class="input-group-btn">
                                      <button class="btn btn-primary btn-sm" type="submit"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                 </span>
                           </div>
                           
               </form> 
                <?php echo form_close(); ?>
            </div>
        </div>
    </header>
    
    
    <div class="panel-body">
         <?php if ($tb_pendaftarans) : ?>
          <table class="table table-hover table-condensed">
              
            <thead>
              <tr>
                <th class="header">#</th>
                
                    <th>Status</th>   
                
                    <th>Namalengkap</th>   
                
                    <th>Namapanggilan</th>   
                
                    <th>Tmptlahir</th>   
                
                    <th>Tgl Lahir</th>   
                
                    <th>Gol Dar</th>   
                
                    <th>Jk</th>   
                
                    <th>Agama</th>   
                
                    <th>Status Diri</th>   
                
                    <th>Kewarganegaraan</th>   
                
                    <th>Alamat</th>   
                
                    <th>Kelurahan</th>   
                
                    <th>Kecamatan</th>   
                
                    <th>Kabupaten</th>   
                
                    <th>Kode Pos</th>   
                
                    <th>Propinsi</th>   
                
                    <th>No Rumah</th>   
                
                    <th>No Hp</th>   
                
                    <th>Pekerjaan</th>   
                
                    <th>Status Penjamin</th>   
                
                    <th>Nama Penjamin</th>   
                
                    <th>Hubungan Penjamin</th>   
                
                    <th>Alamat Penjamin</th>   
                
                    <th>Notelp Penjamin</th>   
                
                <th class="red header" align="right" width="120">Aksi</th>
              </tr>
            </thead>
            
            
            <tbody>
             
               <?php foreach ($tb_pendaftarans as $tb_pendaftaran) : ?>
              <tr>
              	<td><?php echo $number++;; ?> </td>
               
               <td><?php echo $tb_pendaftaran['status_id']; ?></td>
               
               <td><?php echo $tb_pendaftaran['namalengkap']; ?></td>
               
               <td><?php echo $tb_pendaftaran['namapanggilan']; ?></td>
               
               <td><?php echo $tb_pendaftaran['tmptlahir']; ?></td>
               
               <td><?php echo $tb_pendaftaran['tgl_lahir']; ?></td>
               
               <td><?php echo $tb_pendaftaran['gol_dar']; ?></td>
               
               <td><?php echo $tb_pendaftaran['jk']; ?></td>
               
               <td><?php echo $tb_pendaftaran['agama']; ?></td>
               
               <td><?php echo $tb_pendaftaran['status_diri']; ?></td>
               
               <td><?php echo $tb_pendaftaran['kewarganegaraan']; ?></td>
               
               <td><?php echo $tb_pendaftaran['alamat']; ?></td>
               
               <td><?php echo $tb_pendaftaran['kelurahan']; ?></td>
               
               <td><?php echo $tb_pendaftaran['kecamatan']; ?></td>
               
               <td><?php echo $tb_pendaftaran['kabupaten']; ?></td>
               
               <td><?php echo $tb_pendaftaran['kode_pos']; ?></td>
               
               <td><?php echo $tb_pendaftaran['propinsi']; ?></td>
               
               <td><?php echo $tb_pendaftaran['no_rumah']; ?></td>
               
               <td><?php echo $tb_pendaftaran['no_hp']; ?></td>
               
               <td><?php echo $tb_pendaftaran['pekerjaan']; ?></td>
               
               <td><?php echo $tb_pendaftaran['status_penjamin']; ?></td>
               
               <td><?php echo $tb_pendaftaran['nama_penjamin']; ?></td>
               
               <td><?php echo $tb_pendaftaran['hubungan_penjamin']; ?></td>
               
               <td><?php echo $tb_pendaftaran['alamat_penjamin']; ?></td>
               
               <td><?php echo $tb_pendaftaran['notelp_penjamin']; ?></td>
               
                <td>    
                    
                    <?php
                                  echo anchor(
                                          site_url('tb_pendaftaran/show/' . $tb_pendaftaran['id_pendaftaran']),
                                            '<i class="glyphicon glyphicon-eye-open"></i>',
                                            'class="btn btn-sm btn-info" data-tooltip="tooltip" data-placement="top" title="Detail"'
                                          );
                   ?>
                    
                    <?php
                                  echo anchor(
                                          site_url('tb_pendaftaran/edit/' . $tb_pendaftaran['id_pendaftaran']),
                                            '<i class="glyphicon glyphicon-edit"></i>',
                                            'class="btn btn-sm btn-success" data-tooltip="tooltip" data-placement="top" title="Edit"'
                                          );
                   ?>
                   
                   <?php
                                  echo anchor(
                                          site_url('tb_pendaftaran/destroy/' . $tb_pendaftaran['id_pendaftaran']),
                                            '<i class="glyphicon glyphicon-trash"></i>',
                                            'onclick="return confirm(\'Anda yakin..???\');" class="btn btn-sm btn-danger" data-tooltip="tooltip" data-placement="top" title="Hapus"'
                                          );
                   ?>   
                                 
                </td>
              </tr>     
               <?php endforeach; ?>
            </tbody>
          </table>
          <?php else: ?>
                <?php  echo notify('Data tb_pendaftaran belum tersedia','info');?>
          <?php endif; ?>
    </div>
    
    
    <div class="panel-footer">
        <div class="row">
           <div class="col-md-3">
               Tb Pendaftaran
               <span class="label label-info">
                    <?php echo $total; ?>
               </span>
           </div>  
           <div class="col-md-9">
                 <?php echo $pagination; ?>
           </div>
        </div>
    </div>
</section>