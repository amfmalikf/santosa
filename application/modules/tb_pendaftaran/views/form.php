<div class="row">
	<div class="col-lg-12 col-md-12">		
		<?php 
                
                echo create_breadcrumb();		
                echo $this->session->flashdata('notify');
                
                ?>
	</div>
</div><!-- /.row -->

<?php echo form_open(site_url('tb_pendaftaran/' . $action),'role="form" class="form-horizontal" id="form_tb_pendaftaran" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-signal"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="status_id" class="col-sm-2 control-label">Status <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_radio(
                            array(
                                 'name'  => 'status_id',
                                 'id'    => 'status_id',                       
                                 'class' => 'form-control input-sm  required',                                 
                                 ) 
                           );             
                  ?>
                 <?php echo form_error('status_id');?>
                </div>
              </div> <!--/ Status -->
                          
               <div class="form-group">
                   <label for="namalengkap" class="col-sm-2 control-label">Namalengkap <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'namalengkap',
                                 'id'           => 'namalengkap',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Namalengkap',
                                 'maxlength'=>'50'
                                 ),
                                 set_value('namalengkap',$tb_pendaftaran['namalengkap'])
                           );             
                  ?>
                 <?php echo form_error('namalengkap');?>
                </div>
              </div> <!--/ Namalengkap -->
                          
               <div class="form-group">
                   <label for="namapanggilan" class="col-sm-2 control-label">Namapanggilan <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'namapanggilan',
                                 'id'           => 'namapanggilan',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Namapanggilan',
                                 'maxlength'=>'50'
                                 ),
                                 set_value('namapanggilan',$tb_pendaftaran['namapanggilan'])
                           );             
                  ?>
                 <?php echo form_error('namapanggilan');?>
                </div>
              </div> <!--/ Namapanggilan -->
                          
               <div class="form-group">
                   <label for="tmptlahir" class="col-sm-2 control-label">Tmptlahir <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'tmptlahir',
                                 'id'           => 'tmptlahir',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Tmptlahir',
                                 'maxlength'=>'50'
                                 ),
                                 set_value('tmptlahir',$tb_pendaftaran['tmptlahir'])
                           );             
                  ?>
                 <?php echo form_error('tmptlahir');?>
                </div>
              </div> <!--/ Tmptlahir -->
                          
               <div class="form-group">
                   <label for="tgl_lahir" class="col-sm-2 control-label">Tgl Lahir <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'tgl_lahir',
                                 'id'           => 'tgl_lahir',                       
                                 'class'        => 'form-control input-sm tanggal  required',
                                 'placeholder'  => 'Tgl Lahir',
                                 
                                 ),
                                 set_value('tgl_lahir',$tb_pendaftaran['tgl_lahir'])
                           );             
                  ?>
                 <?php echo form_error('tgl_lahir');?>
                </div>
              </div> <!--/ Tgl Lahir -->
                          
               <div class="form-group">
                   <label for="gol_dar" class="col-sm-2 control-label">Gol Dar <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'gol_dar',
                                 'id'           => 'gol_dar',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Gol Dar',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('gol_dar',$tb_pendaftaran['gol_dar'])
                           );             
                  ?>
                 <?php echo form_error('gol_dar');?>
                </div>
              </div> <!--/ Gol Dar -->
                          
               <div class="form-group">
                   <label for="jk" class="col-sm-2 control-label">Jk <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_radio(
                            array(
                                 'name'  => 'jk',
                                 'id'    => 'jk',                       
                                 'class' => 'form-control input-sm  required',                                 
                                 ) 
                           );             
                  ?>
                 <?php echo form_error('jk');?>
                </div>
              </div> <!--/ Jk -->
                          
               <div class="form-group">
                   <label for="agama" class="col-sm-2 control-label">Agama <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_radio(
                            array(
                                 'name'  => 'agama',
                                 'id'    => 'agama',                       
                                 'class' => 'form-control input-sm  required',                                 
                                 ) 
                           );             
                  ?>
                 <?php echo form_error('agama');?>
                </div>
              </div> <!--/ Agama -->
                          
               <div class="form-group">
                   <label for="status_diri" class="col-sm-2 control-label">Status Diri <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_radio(
                            array(
                                 'name'  => 'status_diri',
                                 'id'    => 'status_diri',                       
                                 'class' => 'form-control input-sm  required',                                 
                                 ) 
                           );             
                  ?>
                 <?php echo form_error('status_diri');?>
                </div>
              </div> <!--/ Status Diri -->
                          
               <div class="form-group">
                   <label for="kewarganegaraan" class="col-sm-2 control-label">Kewarganegaraan <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_radio(
                            array(
                                 'name'  => 'kewarganegaraan',
                                 'id'    => 'kewarganegaraan',                       
                                 'class' => 'form-control input-sm  required',                                 
                                 ) 
                           );             
                  ?>
                 <?php echo form_error('kewarganegaraan');?>
                </div>
              </div> <!--/ Kewarganegaraan -->
                          
               <div class="form-group">
                   <label for="alamat" class="col-sm-2 control-label">Alamat <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'alamat',
                                 'id'           => 'alamat',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Alamat',
                                 
                                 ),
                                 set_value('alamat',$tb_pendaftaran['alamat'])
                           );             
                  ?>
                 <?php echo form_error('alamat');?>
                </div>
              </div> <!--/ Alamat -->
                          
               <div class="form-group">
                   <label for="kelurahan" class="col-sm-2 control-label">Kelurahan <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'kelurahan',
                                 'id'           => 'kelurahan',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Kelurahan',
                                 
                                 ),
                                 set_value('kelurahan',$tb_pendaftaran['kelurahan'])
                           );             
                  ?>
                 <?php echo form_error('kelurahan');?>
                </div>
              </div> <!--/ Kelurahan -->
                          
               <div class="form-group">
                   <label for="kecamatan" class="col-sm-2 control-label">Kecamatan <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'kecamatan',
                                 'id'           => 'kecamatan',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Kecamatan',
                                 
                                 ),
                                 set_value('kecamatan',$tb_pendaftaran['kecamatan'])
                           );             
                  ?>
                 <?php echo form_error('kecamatan');?>
                </div>
              </div> <!--/ Kecamatan -->
                          
               <div class="form-group">
                   <label for="kabupaten" class="col-sm-2 control-label">Kabupaten <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'kabupaten',
                                 'id'           => 'kabupaten',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Kabupaten',
                                 
                                 ),
                                 set_value('kabupaten',$tb_pendaftaran['kabupaten'])
                           );             
                  ?>
                 <?php echo form_error('kabupaten');?>
                </div>
              </div> <!--/ Kabupaten -->
                          
               <div class="form-group">
                   <label for="kode_pos" class="col-sm-2 control-label">Kode Pos <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'kode_pos',
                                 'id'           => 'kode_pos',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Kode Pos',
                                 'maxlength'=>'5'
                                 ),
                                 set_value('kode_pos',$tb_pendaftaran['kode_pos'])
                           );             
                  ?>
                 <?php echo form_error('kode_pos');?>
                </div>
              </div> <!--/ Kode Pos -->
                          
               <div class="form-group">
                   <label for="propinsi" class="col-sm-2 control-label">Propinsi <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'propinsi',
                                 'id'           => 'propinsi',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Propinsi',
                                 
                                 ),
                                 set_value('propinsi',$tb_pendaftaran['propinsi'])
                           );             
                  ?>
                 <?php echo form_error('propinsi');?>
                </div>
              </div> <!--/ Propinsi -->
                          
               <div class="form-group">
                   <label for="no_rumah" class="col-sm-2 control-label">No Rumah <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'no_rumah',
                                 'id'           => 'no_rumah',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'No Rumah',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('no_rumah',$tb_pendaftaran['no_rumah'])
                           );             
                  ?>
                 <?php echo form_error('no_rumah');?>
                </div>
              </div> <!--/ No Rumah -->
                          
               <div class="form-group">
                   <label for="no_hp" class="col-sm-2 control-label">No Hp <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'no_hp',
                                 'id'           => 'no_hp',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'No Hp',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('no_hp',$tb_pendaftaran['no_hp'])
                           );             
                  ?>
                 <?php echo form_error('no_hp');?>
                </div>
              </div> <!--/ No Hp -->
                          
               <div class="form-group">
                   <label for="pekerjaan" class="col-sm-2 control-label">Pekerjaan <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'pekerjaan',
                                 'id'           => 'pekerjaan',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Pekerjaan',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('pekerjaan',$tb_pendaftaran['pekerjaan'])
                           );             
                  ?>
                 <?php echo form_error('pekerjaan');?>
                </div>
              </div> <!--/ Pekerjaan -->
                          
               <div class="form-group">
                   <label for="status_penjamin" class="col-sm-2 control-label">Status Penjamin <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_radio(
                            array(
                                 'name'  => 'status_penjamin',
                                 'id'    => 'status_penjamin',                       
                                 'class' => 'form-control input-sm  required',                                 
                                 ) 
                           );             
                  ?>
                 <?php echo form_error('status_penjamin');?>
                </div>
              </div> <!--/ Status Penjamin -->
                          
               <div class="form-group">
                   <label for="nama_penjamin" class="col-sm-2 control-label">Nama Penjamin <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'nama_penjamin',
                                 'id'           => 'nama_penjamin',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Nama Penjamin',
                                 'maxlength'=>'50'
                                 ),
                                 set_value('nama_penjamin',$tb_pendaftaran['nama_penjamin'])
                           );             
                  ?>
                 <?php echo form_error('nama_penjamin');?>
                </div>
              </div> <!--/ Nama Penjamin -->
                          
               <div class="form-group">
                   <label for="hubungan_penjamin" class="col-sm-2 control-label">Hubungan Penjamin <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'hubungan_penjamin',
                                 'id'           => 'hubungan_penjamin',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Hubungan Penjamin',
                                 'maxlength'=>'50'
                                 ),
                                 set_value('hubungan_penjamin',$tb_pendaftaran['hubungan_penjamin'])
                           );             
                  ?>
                 <?php echo form_error('hubungan_penjamin');?>
                </div>
              </div> <!--/ Hubungan Penjamin -->
                          
               <div class="form-group">
                   <label for="alamat_penjamin" class="col-sm-2 control-label">Alamat Penjamin <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'alamat_penjamin',
                                 'id'           => 'alamat_penjamin',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Alamat Penjamin',
                                 'maxlength'=>'50'
                                 ),
                                 set_value('alamat_penjamin',$tb_pendaftaran['alamat_penjamin'])
                           );             
                  ?>
                 <?php echo form_error('alamat_penjamin');?>
                </div>
              </div> <!--/ Alamat Penjamin -->
                          
               <div class="form-group">
                   <label for="notelp_penjamin" class="col-sm-2 control-label">Notelp Penjamin <span class="required-input">*</span></label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'notelp_penjamin',
                                 'id'           => 'notelp_penjamin',                       
                                 'class'        => 'form-control input-sm  required',
                                 'placeholder'  => 'Notelp Penjamin',
                                 'maxlength'=>'15'
                                 ),
                                 set_value('notelp_penjamin',$tb_pendaftaran['notelp_penjamin'])
                           );             
                  ?>
                 <?php echo form_error('notelp_penjamin');?>
                </div>
              </div> <!--/ Notelp Penjamin -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0">
                   <a href="<?php echo site_url('tb_pendaftaran'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  