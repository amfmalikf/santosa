<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tb_dokter
 * @created on : Thursday, 02-Aug-2018 13:47:22
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class tb_dokter extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('tb_dokters');
    }
    

    /**
    * List all data tb_dokter
    *
    */
    public function index() 
    {
        $config = array(
            'base_url'          => site_url('tb_dokter/index/'),
            'total_rows'        => $this->tb_dokters->count_all(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
            
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['pagination']     = $this->pagination->create_links();
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['tb_dokters']       = $this->tb_dokters->get_all($config['per_page'], $this->uri->segment(3));
        $this->template->render('tb_dokter/view',$data);
	      
    }

    

    /**
    * Call Form to Add  New tb_dokter
    *
    */
    public function add() 
    {       
        $data['tb_dokter'] = $this->tb_dokters->add();
        $data['action']  = 'tb_dokter/save';
        $data['rev']=$this->tb_dokters->sms_get_spesialis();
     
        $this->template->js_add('
                $(document).ready(function(){
                // binds form submission and fields to the validation engine
                $("#form_tb_dokter").parsley();
                        });','embed');
      
        $this->template->render('tb_dokter/form',$data);

    }

    

    /**
    * Call Form to Modify tb_dokter
    *
    */
    public function edit($id='') 
    {
        if ($id != '') 
        {

            $data['tb_dokter']      = $this->tb_dokters->get_one($id);
            $data['action']       = 'tb_dokter/save/' . $id;           
      
            $this->template->js_add('
                     $(document).ready(function(){
                    // binds form submission and fields to the validation engine
                    $("#form_tb_dokter").parsley();
                                    });','embed');
            
            $this->template->render('tb_dokter/form',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_dokter'));
        }
    }


    
    /**
    * Save & Update data  tb_dokter
    *
    */
    public function save($id =NULL) 
    {
        // validation config
        $config = array(
                  
                    array(
                        'field' => 'id_spesialis',
                        'label' => 'Id Spesialis',
                        'rules' => 'trim|xss_clean'
                        ),
                    
                    array(
                        'field' => 'nama_dokter',
                        'label' => 'Nama Dokter',
                        'rules' => 'trim|xss_clean'
                        ),
                               
                  );
            
        // if id NULL then add new data
        if(!$id)
        {    
                  $this->form_validation->set_rules($config);

                  if ($this->form_validation->run() == TRUE) 
                  {
                      if ($this->input->post()) 
                      {
                          
                          $this->tb_dokters->save();
                          $this->session->set_flashdata('notif', notify('Data berhasil di simpan','success'));
                          redirect('tb_dokter');
                      }
                  } 
                  else // If validation incorrect 
                  {
                      $this->add();
                  }
         }
         else // Update data if Form Edit send Post and ID available
         {               
                $this->form_validation->set_rules($config);

                if ($this->form_validation->run() == TRUE) 
                {
                    if ($this->input->post()) 
                    {
                        $this->tb_dokters->update($id);
                        $this->session->set_flashdata('notif', notify('Data berhasil di update','success'));
                        redirect('tb_dokter');
                    }
                } 
                else // If validation incorrect 
                {
                    $this->edit($id);
                }
         }
    }

    
    
    /**
    * Detail tb_dokter
    *
    */
    public function show($id='') 
    {
        if ($id != '') 
        {

            $data['tb_dokter'] = $this->tb_dokters->get_one($id);            
            $this->template->render('tb_dokter/_show',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_dokter'));
        }
    }
    
    
    /**
    * Search tb_dokter like ""
    *
    */   
    public function search()
    {
        if($this->input->post('q'))
        {
            $keyword = $this->input->post('q');
            
            $this->session->set_userdata(
                        array('keyword' => $this->input->post('q',TRUE))
                    );
        }
        
         $config = array(
            'base_url'          => site_url('tb_dokter/search/'),
            'total_rows'        => $this->tb_dokters->count_all_search(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['pagination']     = $this->pagination->create_links();
        $data['tb_dokters']       = $this->tb_dokters->get_search($config['per_page'], $this->uri->segment(3));
       
        $this->template->render('tb_dokter/view',$data);
    }
    
    
    /**
    * Delete tb_dokter by ID
    *
    */
    public function destroy($id) 
    {        
        if ($id) 
        {
            $this->tb_dokters->destroy($id);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('tb_dokter');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('tb_dokter');
        }       
    }

}

?>
