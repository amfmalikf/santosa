<div class="row">
	<div class="col-lg-12 col-md-12">		
		<?php 
                
                echo create_breadcrumb();		
                echo $this->session->flashdata('notify');
                
                ?>
	</div>
</div><!-- /.row -->

<?php echo form_open(site_url('tb_dokter/' . $action),'role="form" class="form-horizontal" id="form_tb_dokter" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-signal"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="id_spesialis" class="col-sm-2 control-label">Pilih Spesialis</label>
                <div class="col-sm-6">                                   
                <select name="id_spesialis" class="form-control pilih">
                  <option value="">-- Silahkan pilih --</option>
                  <?php foreach($rev as $row2) { ?>
                                <option  <?php if ($row2['id_spesialis']==$row2['id_spesialis']) ?> value="<?php echo $row2['id_spesialis'];?>"><?php echo $row2['jenis_spesialis']?></option> 
                              <?php } ?>
                </select> 

                </div>
              </div> <!--/ Id Spesialis -->
                          
               <div class="form-group">
                   <label for="nama_dokter" class="col-sm-2 control-label">Nama Dokter</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'nama_dokter',
                                 'id'           => 'nama_dokter',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Nama Dokter',
                                 
                                 ),
                                 set_value('nama_dokter',$tb_dokter['nama_dokter'])
                           );             
                  ?>
                 <?php echo form_error('nama_dokter');?>
                </div>
              </div> <!--/ Nama Dokter -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0">
                   <a href="<?php echo site_url('tb_dokter'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  