<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_dokter
 * @created on : Thursday, 02-Aug-2018 13:47:22
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class tb_dokters extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tb_dokter
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all($limit, $offset) 
    {
        $this->db->select('*');
        $this->db->from('tb_dokter a');
        $this->db->join('tb_spesialis b','a.id_spesialis=b.id_spesialis');
        $query = $this->db->get();
        return $query->result_array();
    }

    

    /**
     *  Count All tb_dokter
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tb_dokter');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tb_dokter
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
                
        $this->db->like('nama_dokter', $keyword);  
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tb_dokter');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tb_dokter
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tb_dokter');        
                
        $this->db->like('nama_dokter', $keyword);  
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tb_dokter
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_dokter', $id);
        $result = $this->db->get('tb_dokter');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tb_dokter
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'id_spesialis' => '',
            
                'nama_dokter' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'id_spesialis' => strip_tags($this->input->post('id_spesialis', TRUE)),
        
            'nama_dokter' => strip_tags($this->input->post('nama_dokter', TRUE)),
        
        );
        
        
        $this->db->insert('tb_dokter', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'id_spesialis' => strip_tags($this->input->post('id_spesialis', TRUE)),
        
                'nama_dokter' => strip_tags($this->input->post('nama_dokter', TRUE)),
        
        );
        
        
        $this->db->where('id_dokter', $id);
        $this->db->update('tb_dokter', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_dokter', $id);
        $this->db->delete('tb_dokter');
        
    }

public function sms_get_spesialis() 
    {

        $result = $this->db->get('tb_spesialis');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }







    



}
