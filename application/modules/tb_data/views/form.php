<div class="row">
	<div class="col-lg-12 col-md-12">		
		<?php 
                
                echo create_breadcrumb();		
                echo $this->session->flashdata('notify');
                
                ?>
	</div>
</div><!-- /.row -->

<?php echo form_open(site_url('tb_data/' . $action),'role="form" class="form-horizontal" id="form_tb_data" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-signal"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="id_pendaftaran" class="col-sm-2 control-label">Id Pendaftaran</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'id_pendaftaran',
                                 'id'           => 'id_pendaftaran',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Id Pendaftaran',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('id_pendaftaran',$tb_data['id_pendaftaran'])
                           );             
                  ?>
                 <?php echo form_error('id_pendaftaran');?>
                </div>
              </div> <!--/ Id Pendaftaran -->
                          
               <div class="form-group">
                   <label for="id_spesialis" class="col-sm-2 control-label">Id Spesialis</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'id_spesialis',
                                 'id'           => 'id_spesialis',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Id Spesialis',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('id_spesialis',$tb_data['id_spesialis'])
                           );             
                  ?>
                 <?php echo form_error('id_spesialis');?>
                </div>
              </div> <!--/ Id Spesialis -->
                          
               <div class="form-group">
                   <label for="id_dokter" class="col-sm-2 control-label">Id Dokter</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'id_dokter',
                                 'id'           => 'id_dokter',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Id Dokter',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('id_dokter',$tb_data['id_dokter'])
                           );             
                  ?>
                 <?php echo form_error('id_dokter');?>
                </div>
              </div> <!--/ Id Dokter -->
                          
               <div class="form-group">
                   <label for="tgl_daftar" class="col-sm-2 control-label">Tgl Daftar</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'tgl_daftar',
                                 'id'           => 'tgl_daftar',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Tgl Daftar',
                                 
                                 ),
                                 set_value('tgl_daftar',$tb_data['tgl_daftar'])
                           );             
                  ?>
                 <?php echo form_error('tgl_daftar');?>
                </div>
              </div> <!--/ Tgl Daftar -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0">
                   <a href="<?php echo site_url('tb_data'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  