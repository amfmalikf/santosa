<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_data
 * @created on : Friday, 17-Aug-2018 19:41:14
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class tb_datas extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tb_data
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all($limit, $offset) 
    {

        $result = $this->db->get('tb_data', $limit, $offset);

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    

    /**
     *  Count All tb_data
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tb_data');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tb_data
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tb_data');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tb_data
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tb_data');        
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tb_data
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_data', $id);
        $result = $this->db->get('tb_data');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tb_data
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'id_pendaftaran' => '',
            
                'id_spesialis' => '',
            
                'id_dokter' => '',
            
                'tgl_daftar' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'id_pendaftaran' => strip_tags($this->input->post('id_pendaftaran', TRUE)),
        
            'id_spesialis' => strip_tags($this->input->post('id_spesialis', TRUE)),
        
            'id_dokter' => strip_tags($this->input->post('id_dokter', TRUE)),
        
            'tgl_daftar' => strip_tags($this->input->post('tgl_daftar', TRUE)),
        
        );
        
        
        $this->db->insert('tb_data', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'id_pendaftaran' => strip_tags($this->input->post('id_pendaftaran', TRUE)),
        
                'id_spesialis' => strip_tags($this->input->post('id_spesialis', TRUE)),
        
                'id_dokter' => strip_tags($this->input->post('id_dokter', TRUE)),
        
                'tgl_daftar' => strip_tags($this->input->post('tgl_daftar', TRUE)),
        
        );
        
        
        $this->db->where('id_data', $id);
        $this->db->update('tb_data', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_data', $id);
        $this->db->delete('tb_data');
        
    }







    



}
