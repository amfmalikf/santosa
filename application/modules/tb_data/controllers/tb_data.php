<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tb_data
 * @created on : Friday, 17-Aug-2018 19:41:14
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class tb_data extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('tb_datas');
    }
    

    /**
    * List all data tb_data
    *
    */
    public function index() 
    {
        $config = array(
            'base_url'          => site_url('tb_data/index/'),
            'total_rows'        => $this->tb_datas->count_all(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
            
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['pagination']     = $this->pagination->create_links();
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['tb_datas']       = $this->tb_datas->get_all($config['per_page'], $this->uri->segment(3));
        $this->template->render('tb_data/view',$data);
	      
    }

    

    /**
    * Call Form to Add  New tb_data
    *
    */
    public function add() 
    {       
        $data['tb_data'] = $this->tb_datas->add();
        $data['action']  = 'tb_data/save';
     
        $this->template->js_add('
                $(document).ready(function(){
                // binds form submission and fields to the validation engine
                $("#form_tb_data").parsley();
                        });','embed');
      
        $this->template->render('tb_data/form',$data);

    }

    

    /**
    * Call Form to Modify tb_data
    *
    */
    public function edit($id='') 
    {
        if ($id != '') 
        {

            $data['tb_data']      = $this->tb_datas->get_one($id);
            $data['action']       = 'tb_data/save/' . $id;           
      
            $this->template->js_add('
                     $(document).ready(function(){
                    // binds form submission and fields to the validation engine
                    $("#form_tb_data").parsley();
                                    });','embed');
            
            $this->template->render('tb_data/form',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_data'));
        }
    }


    
    /**
    * Save & Update data  tb_data
    *
    */
    public function save($id =NULL) 
    {
        // validation config
        $config = array(
                  
                    array(
                        'field' => 'id_pendaftaran',
                        'label' => 'Id Pendaftaran',
                        'rules' => 'trim|xss_clean'
                        ),
                    
                    array(
                        'field' => 'id_spesialis',
                        'label' => 'Id Spesialis',
                        'rules' => 'trim|xss_clean'
                        ),
                    
                    array(
                        'field' => 'id_dokter',
                        'label' => 'Id Dokter',
                        'rules' => 'trim|xss_clean'
                        ),
                    
                    array(
                        'field' => 'tgl_daftar',
                        'label' => 'Tgl Daftar',
                        'rules' => 'trim|xss_clean'
                        ),
                               
                  );
            
        // if id NULL then add new data
        if(!$id)
        {    
                  $this->form_validation->set_rules($config);

                  if ($this->form_validation->run() == TRUE) 
                  {
                      if ($this->input->post()) 
                      {
                          
                          $this->tb_datas->save();
                          $this->session->set_flashdata('notif', notify('Data berhasil di simpan','success'));
                          redirect('tb_data');
                      }
                  } 
                  else // If validation incorrect 
                  {
                      $this->add();
                  }
         }
         else // Update data if Form Edit send Post and ID available
         {               
                $this->form_validation->set_rules($config);

                if ($this->form_validation->run() == TRUE) 
                {
                    if ($this->input->post()) 
                    {
                        $this->tb_datas->update($id);
                        $this->session->set_flashdata('notif', notify('Data berhasil di update','success'));
                        redirect('tb_data');
                    }
                } 
                else // If validation incorrect 
                {
                    $this->edit($id);
                }
         }
    }

    
    
    /**
    * Detail tb_data
    *
    */
    public function show($id='') 
    {
        if ($id != '') 
        {

            $data['tb_data'] = $this->tb_datas->get_one($id);            
            $this->template->render('tb_data/_show',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_data'));
        }
    }
    
    
    /**
    * Search tb_data like ""
    *
    */   
    public function search()
    {
        if($this->input->post('q'))
        {
            $keyword = $this->input->post('q');
            
            $this->session->set_userdata(
                        array('keyword' => $this->input->post('q',TRUE))
                    );
        }
        
         $config = array(
            'base_url'          => site_url('tb_data/search/'),
            'total_rows'        => $this->tb_datas->count_all_search(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['pagination']     = $this->pagination->create_links();
        $data['tb_datas']       = $this->tb_datas->get_search($config['per_page'], $this->uri->segment(3));
       
        $this->template->render('tb_data/view',$data);
    }
    
    
    /**
    * Delete tb_data by ID
    *
    */
    public function destroy($id) 
    {        
        if ($id) 
        {
            $this->tb_datas->destroy($id);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('tb_data');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('tb_data');
        }       
    }

}

?>
