<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_poli
 * @created on : Monday, 09-Jul-2018 14:13:27
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class tb_polis extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tb_poli
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all($limit, $offset) 
    {

        $result = $this->db->get('tb_poli', $limit, $offset);

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    

    /**
     *  Count All tb_poli
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tb_poli');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tb_poli
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
                
        $this->db->like('nama_poli', $keyword);  
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tb_poli');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tb_poli
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tb_poli');        
                
        $this->db->like('nama_poli', $keyword);  
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tb_poli
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_poli', $id);
        $result = $this->db->get('tb_poli');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tb_poli
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'nama_poli' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'nama_poli' => strip_tags($this->input->post('nama_poli', TRUE)),
        
        );
        
        
        $this->db->insert('tb_poli', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'nama_poli' => strip_tags($this->input->post('nama_poli', TRUE)),
        
        );
        
        
        $this->db->where('id_poli', $id);
        $this->db->update('tb_poli', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_poli', $id);
        $this->db->delete('tb_poli');
        
    }







    



}
