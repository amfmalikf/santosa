<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tb_poli
 * @created on : Monday, 09-Jul-2018 14:13:27
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class tb_poli extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('tb_polis');
    }
    

    /**
    * List all data tb_poli
    *
    */
    public function index() 
    {
        $config = array(
            'base_url'          => site_url('tb_poli/index/'),
            'total_rows'        => $this->tb_polis->count_all(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
            
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['pagination']     = $this->pagination->create_links();
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['tb_polis']       = $this->tb_polis->get_all($config['per_page'], $this->uri->segment(3));
        $this->template->render('tb_poli/view',$data);
	      
    }

    

    /**
    * Call Form to Add  New tb_poli
    *
    */
    public function add() 
    {       
        $data['tb_poli'] = $this->tb_polis->add();
        $data['action']  = 'tb_poli/save';
     
        $this->template->js_add('
                $(document).ready(function(){
                // binds form submission and fields to the validation engine
                $("#form_tb_poli").parsley();
                        });','embed');
      
        $this->template->render('tb_poli/form',$data);

    }

    

    /**
    * Call Form to Modify tb_poli
    *
    */
    public function edit($id='') 
    {
        if ($id != '') 
        {

            $data['tb_poli']      = $this->tb_polis->get_one($id);
            $data['action']       = 'tb_poli/save/' . $id;           
      
            $this->template->js_add('
                     $(document).ready(function(){
                    // binds form submission and fields to the validation engine
                    $("#form_tb_poli").parsley();
                                    });','embed');
            
            $this->template->render('tb_poli/form',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_poli'));
        }
    }


    
    /**
    * Save & Update data  tb_poli
    *
    */
    public function save($id =NULL) 
    {
        // validation config
        $config = array(
                  
                    array(
                        'field' => 'nama_poli',
                        'label' => 'Nama Poli',
                        'rules' => 'trim|xss_clean|required'
                        ),
                               
                  );
            
        // if id NULL then add new data
        if(!$id)
        {    
                  $this->form_validation->set_rules($config);

                  if ($this->form_validation->run() == TRUE) 
                  {
                      if ($this->input->post()) 
                      {
                          
                          $this->tb_polis->save();
                          $this->session->set_flashdata('notif', notify('Data berhasil di simpan','success'));
                          redirect('tb_poli');
                      }
                  } 
                  else // If validation incorrect 
                  {
                      $this->add();
                  }
         }
         else // Update data if Form Edit send Post and ID available
         {               
                $this->form_validation->set_rules($config);

                if ($this->form_validation->run() == TRUE) 
                {
                    if ($this->input->post()) 
                    {
                        $this->tb_polis->update($id);
                        $this->session->set_flashdata('notif', notify('Data berhasil di update','success'));
                        redirect('tb_poli');
                    }
                } 
                else // If validation incorrect 
                {
                    $this->edit($id);
                }
         }
    }

    
    
    /**
    * Detail tb_poli
    *
    */
    public function show($id='') 
    {
        if ($id != '') 
        {

            $data['tb_poli'] = $this->tb_polis->get_one($id);            
            $this->template->render('tb_poli/_show',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','info'));
            redirect(site_url('tb_poli'));
        }
    }
    
    
    /**
    * Search tb_poli like ""
    *
    */   
    public function search()
    {
        if($this->input->post('q'))
        {
            $keyword = $this->input->post('q');
            
            $this->session->set_userdata(
                        array('keyword' => $this->input->post('q',TRUE))
                    );
        }
        
         $config = array(
            'base_url'          => site_url('tb_poli/search/'),
            'total_rows'        => $this->tb_polis->count_all_search(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['pagination']     = $this->pagination->create_links();
        $data['tb_polis']       = $this->tb_polis->get_search($config['per_page'], $this->uri->segment(3));
       
        $this->template->render('tb_poli/view',$data);
    }
    
    
    /**
    * Delete tb_poli by ID
    *
    */
    public function destroy($id) 
    {        
        if ($id) 
        {
            $this->tb_polis->destroy($id);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('tb_poli');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('tb_poli');
        }       
    }

}

?>
