<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tabel_paket
 * @created on : Thursday, 09-Nov-2017 13:45:42
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2017
 *
 *
 */


class dashboard extends MY_Controller
{
 
    public function __construct() 
    { 
        parent::__construct();         
        $this->load->model('dasboards');
    }
    public function index(){
        $data['jml_dokter'] = $this->dasboards->count_dokter();
        $data['jml_spesialis'] = $this->dasboards->count_spesialis();
        $data['jml_pendaftar'] = $this->dasboards->count_pendaftar();
        $data['jml_permintaan'] = $this->dasboards->count_permintaan();
    	$this->template->render('view',$data);

    }

	function show_permintaan(){
	$this->template->render('detail_permintaan',$data);
    $data['daper'] = $this->dasboards->data_permintaan();

	}




}