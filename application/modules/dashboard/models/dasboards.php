<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_dokter
 * @created on : Thursday, 02-Aug-2018 13:47:22
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class dasboards extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tb_dokter
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */


public function count_dokter(){
        $query = $this->db->query("SELECT count(id_dokter) as dok FROM tb_dokter");
        return $query->row_array();
	}

public function count_spesialis(){
        $query = $this->db->query("SELECT count(id_spesialis) as spe FROM tb_spesialis");
        return $query->row_array();
	}

public function count_pendaftar(){
        $query = $this->db->query("SELECT count(id_pendaftaran) as daf FROM tb_pendaftaran");
        return $query->row_array();
	}

public function count_permintaan(){
        $query = $this->db->query("SELECT count(id_data) as dat FROM tb_data");
        return $query->row_array();
	}

public function data_permintaan(){
        $query = $this->db->query("SELECT * FROM tb_data a JOIN tb_pendaftaran b ON a.id_pendaftaran=b.id_pendaftaran JOIN tb_spesialis c ON a.id_spesialis=c.id_spesialis JOIN tb_dokter d ON a.id_dokter=d.id_dokter");
        return $query->row_array();
	}

}